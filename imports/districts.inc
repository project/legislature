<?php

function legislature_import_districts($name) {
  $file = file_save_upload('data');
  $handle = fopen($file->filepath, 'r');

  // Check the header
  $header = array(
    'jurisdiction',
    'office',
    'number',
    'state',
    'name_display',
  );
  $row = fgetcsv($handle);
  foreach ($row as $n => $column) {
    if ($column !== $header[$n]) {
      drupal_set_message(t('Column headers are not correct.'), 'error');
      return;
    }
  }

  while ($row = fgetcsv($handle)) {
    $district = array();
    foreach ($row as $n => $column) {
      $district[$header[$n]] = trim($column);
    }
    legislature_district($district['jurisdiction'], $district['office'], $district['state'], $district['number'], $district['name_display']);
  }

  fclose($handle);
  file_delete($file->filepath);
}
