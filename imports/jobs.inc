<?php

function legislature_import_jobs($name) {
  $file = file_save_upload('data');
  $handle = fopen($file->filepath, 'r');

  // Check the header
  $header = array(
    'politician_id',
    'jurisdiction',
    'state',
    'office',
    'number',
    'start',
    'end',
    'is_candidate',
  );
  $row = fgetcsv($handle);
  foreach ($row as $n => $column) {
    if ($column !== $header[$n]) {
      drupal_set_message(t('Column headers are not correct.'), 'error');
      return;
    }
  }

  $politician_jobs = array();
  while ($row = fgetcsv($handle)) {
    $job = array();
    foreach ($row as $n => $column) {
      $job[$header[$n]] = trim($column);
    }

    list($year, $month, $day) = explode('-', $job['start']);
    $start = gmmktime(0, 0, 0, $month, $day, $year);
    list($year, $month, $day) = explode('-', $job['end']);
    $end = gmmktime(0, 0, 0, $month, $day, $year);

    if (!isset($politician_jobs[$job['politician_id']])) {
      $politician_jobs[$job['politician_id']] = array();
    }
    $politician_jobs[$job['politician_id']][] = array(
      'office_id' => legislature_get_office_id($job['jurisdiction'], $job['office']),
      'district_id' => legislature_get_district_id($job['jurisdiction'], $job['state'], $job['office'], $job['number']),
      'start' => $start,
      'end' => $end,
      'is_candidate' => $job['is_candidate'],
    );
  }

  foreach ($politician_jobs as $politician_id => $jobs) {
    legislature_save_elected_jobs($job['jurisdiction'], $politician_id, $jobs);
  }

  fclose($handle);
  file_delete($file->filepath);
}
