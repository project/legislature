<?php

function legislature_ia_bills() {
  set_time_limit(0);
  foreach (legislature_get_enabled_sessions('ia') as $session) {
    $response = drupal_http_request('http://coolice.legis.state.ia.us/Cool-ICE/default.asp?category=billinfo&service=Billbook&frm=2&menu=true&ga='. $session);
    preg_match_all('/<OPTION VALUE="http:\/\/coolice\.legis\.state\.ia\.us\/Cool-ICE\/default\.asp\?Category=billinfo&Service=Billbook&menu=true&ga=([0-9]*)&hbill=([A-Z]*)([0-9]*)/', $response->data, $matches);
    foreach ($matches[2] as $key => $prefix) {
      // Get all bills, and not ammendments.
      if (in_array($prefix, array('SF', 'CCS', 'SCR', 'SJR', 'SR', 'SSB', 'HF', 'CCR', 'HCR', 'HJR', 'HR', 'HSB'))) {
        legislature_ia_bill($matches[1][$key], $prefix, $matches[3][$key]);
      }
    }
  }
}

function legislature_ia_bill($session, $prefix, $number) {
  $session = legislature_get_session('ia', $session);
  $bill = array(
    'session_id' => $session->session_id,
    'measure' => $prefix .' '. $number,
    'prefix' => $prefix,
    'description' => '', // Not provided by IA
    'status' => '', // todo
  );
  $data = array();
  if (in_array($bill['prefix'], array('SF', 'CCS', 'SCR', 'SJR', 'SR', 'SSB'))) {
    $data['house'] = 'Senate';
  }
  else if (in_array($bill['prefix'], array('HF', 'CCR', 'HCR', 'HJR', 'HR', 'HSB'))) {
    $data['house'] = 'House';
  }

  $response = drupal_http_request('http://coolice.legis.state.ia.us/Cool-ICE/default.asp?Category=BillInfo&Service=Billbook&ga='. $session->name .'&menu=text&hbill='. $prefix . $number);
  legislature_ia_bill_text($response->data, $data);

  $response = drupal_http_request($data['history url']);
  $bill['last_version'] = md5($response->data);
  legislature_ia_bill_history($response->data, $bill, $data);

  if (legislature_save_bill($bill)) {
    // Save sponsors
    db_query('DELETE FROM {legislature_bill_sponsor} WHERE bill_id = %d', $bill['bill_id']);
    db_query('INSERT INTO {legislature_bill_sponsor} (bill_id, politician_id, type) VALUES (%d, %d, %d)', $bill['bill_id'], array_shift($data['sponsors']), LEGISLATURE_SPONSOR);
    foreach ($data['sponsors'] as $politician_id) {
      db_query('INSERT INTO {legislature_bill_sponsor} (bill_id, politician_id, type) VALUES (%d, %d, %d)', $bill['bill_id'], $politician_id, LEGISLATURE_COSPONSOR);
    }
  }

  /*
  foreach ($bill_history as $bill_history_entry) {
    $links = $bill_history_entry["journal_links"][0];

    $description = $bill_history_entry['description"];
    $date = $bill_history_entry["date"];

    if (!empty($bill_history_entry["location"]) && !empty($links)) {
      // 1: House is NOT empty    --> outcome is NOT empty, -->  documents (links) are NOT empty
      $journal_links = join(" , ", $links);
      $house = $bill_history_entry["location"];
      $ayes = $bill_history_entry["outcome"]["ayes"];
      $nays = $bill_history_entry["outcome"]["nays"];
      $pass_fail = $bill_history_entry["outcome"]["pass_fail"];

      db_query("INSERT INTO {legislature_action} (bill_id, location, description, date, ayes, nays, outcome, documents) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", $bill_id, $house, $description, $date, $ayes, $nays, $pass_fail, $journal_links);
    }
    elseif (!empty($bill_history_entry["location"]) && empty($links)) {
      // 2: House is NOT empty    --> outcome is NOT empty, -->  documents (links) are empty
      $journal_links = '';
      $house = $bill_history_entry["location"];
      db_query("INSERT INTO {legislature_action} (bill_id, location, description, date, documents) VALUES ('%s', '%s', '%s', '%s')", $bill_id, $house, $description, $date, $journal_links);
    }
    elseif (empty($bill_history_entry["location"]) && !empty($links)) {
      // 3. House is empty        --> outcome is empty,     -->  documents (links) are NOT empty
      $journal_links = join(" , ", $links);
      db_query("INSERT INTO {legislature_action} (bill_id, description, date, documents) VALUES ('%s', '%s', '%s', '%s')", $bill_id, $description, $date, $journal_links);
    }
    else {
      // 4. House is empty        --> outcome is empty,     -->  documents (links) are empty
      $journal_links = '';
      db_query("INSERT INTO {legislature_action} (bill_id, description, date, documents) VALUES ('%s', '%s', '%s', '%s')", $bill_id, $description, $date, $journal_links);
    }
  }
  */
}

/**
 * Extract the bill text, which is between <HR> tags.
 */
function legislature_ia_bill_text($html_page, &$data) {
  include_once './'. drupal_get_path('module', 'legislature') .'/imports/ia/htmlparser_extension.php';
  $parser = new LegislatureIaHtmlParser($html_page);

  while ($parser->parse()) {
    if ($parser->is_tag($parser, "HR") && $parser->is_start_tag($parser)) {
      $lines = explode("\n", $parser->get_inner_text($parser, "HR"));
    }
    if ($parser->is_tag($parser, "A") && $parser->is_start_tag($parser)) {
      $href = $parser->iNodeAttributes["href"];
      if ($parser->get_inner_text($parser, 'A') == 'Complete Bill History') {
        $data['history url'] = $href;
      }
    }
  }

  // Removing unecessary header and footer of the page
  $bill_with_footer = array_slice($lines, 6, count($lines));
  $bill = array_slice($bill_with_footer, 0, count($bill_with_footer) - 4);

  $bill_text = array();
  foreach ($bill as $line) {
    if (strcmp($line, '') > 0) {
      $bill_text[] = $line;
    }
  }
  $data['text'] = implode("\n", $bill_text);
}

/**
 * Given a bill history url, this function extracts the history of a bill where
 * each line of history information is stored in an array and this array is
 * stored in the bill history array. The reason for this is to make it easy to
 * store in a db (i.e.: Dates, Action, URL for SJ for given date).
 */
function legislature_ia_bill_history($html_page, &$bill, &$data) {
  static $passed = array(
    'Passed' => 'PASS',
    'Failed to pass' => 'FAIL',
  );

  $xml = simplexml_load_string(tidy_repair_string($html_page, array('output-xhtml' => TRUE)));

  $data['sponsors'] = array();
  foreach (preg_split('/,\s+|\s+and\s+/', preg_replace('/^By (.*)/', '\1', rtrim($xml->body->table[0]->tr[1]->td->font, '.'))) as $name) {
    $name = preg_split('/\s+/', $name);
    if (count($name) == 1) {
      $politician_id = db_result(db_query("SELECT p.politician_id FROM {legislature_politician} p LEFT JOIN {legislature_elected_job} ej ON ej.politician_id = p.politician_id INNER JOIN {legislature_elected_job_session} ejs ON ejs.elected_job_id = ej.elected_job_id AND ejs.session_id = %d INNER JOIN {legislature_office} o ON o.office_id = ej.office_id AND o.jurisdiction = 'IA' AND o.title = '%s' WHERE p.last_name = '%s'", $bill['session_id'], $data['house'], $name[0]));
    }
    else {
      $politician_id = db_result(db_query("SELECT p.politician_id FROM {legislature_politician} p LEFT JOIN {legislature_elected_job} ej ON ej.politician_id = p.politician_id INNER JOIN {legislature_elected_job_session} ejs ON ejs.elected_job_id = ej.elected_job_id AND ejs.session_id = %d INNER JOIN {legislature_office} o ON o.office_id = ej.office_id AND o.jurisdiction = 'IA' AND o.title = '%s' WHERE p.full_name LIKE '%s%'", $bill['session_id'], $data['house'], substr($name[1], 0, -1) .'% '. $name[0]));
    }
    if ($politician_id === FALSE) {
      watchdog('legislature', t('Failed to find Iowa bill sponsor %name', array('%name' => implode(' ', $name))), WATCHDOG_WARNING);
    }
    else {
      $data['sponsors'][] = $politician_id;
    }
  }

  $bill['topic'] = preg_replace('/\s+/', ' ', $xml->body->table[0]->tr[2]->td->font);

  // Start parsing <tr> tags and creating bill history array where an element = Date, Event description
  $data['actions'] = array();
  foreach ($xml->body->table[1]->tr as $row) {
    $action = array(
      'date' => date_parse(preg_replace('/\s+/', ' ', $row->td[0]->font)),
      'description' => trim(preg_replace('/<[^>]*>([^<.,]*).*/', '\1', preg_replace('/\s+/', ' ', $row->td[1]->font->asXML()))),
      'motion' => preg_replace('/\s+/', ' ', strip_tags($row->td[1]->font->asXML())),
      'location' => '',
      'outcome' => '',
      'is_vote' => FALSE,
      'is_substantive' => FALSE,
      'ayes' => 0,
      'noes' => 0,
    );

    if (preg_match('/END OF \d* ACTIONS/', $action['motion'])) {
      continue;
    }

    $action['date_int'] = gmmktime(0, 0, 0, $action['date']['month'], $action['date']['day'], $action['date']['year']);
    $action['date'] = gmdate('Ymd', $action['date_int']);

    // Look for votes
    if (preg_match("/(Passed|Failed to pass) (House|Senate), ayes (\d+), nays (\d+)\./", $action['motion'], $matches) == 1) {
      $action['is_vote'] = TRUE;
      $action['is_substantive'] = TRUE;
      $action['location'] = $matches[2];
      $action['outcome'] = $passed[$matches[1]];
      $action['ayes'] = $matches[3];
      $action['noes'] = $matches[4];

      $journal = drupal_http_request(preg_replace('/#.*/', '', $row->td[1]->font->a['href']));
      $result_string = legislature_pdf2string($journal->data);

      $votes = array(
        'Yeas'    => NULL,
        'Nays'    => NULL,
        'Present' => NULL,
        'Absent'  => NULL,
      );

      $measure = str_replace(array('HF', 'SF'), array('H.F.', 'S.F.'), $bill['measure']);

      // Extract text lines after the S.F bill number to ensure we get
      // the lines that correspond to the correct bill number
      $entries = array();
      foreach (explode('On the question', $result_string) as $entry) {
        if (preg_match("/\(". $measure ."\)/", $entry) == 1) {
          array_push($entries, $entry);
        }
      }

      // Extract text lines based on 2 conditions, we've seen "Shall the bill pass" and S.F. (bill number)
      foreach($entries as $value) {
        if (preg_match("/Shall the bill pass?/", $value) == 1 && preg_match('/'. $measure .'/', $value) == 1) {
          $split_result_string_on_the_bill = explode("The bill", $value);
        }
      }
      $unformatted_text = $split_result_string_on_the_bill[0];
      $unformatted_text = preg_replace("/\d+ JOURNAL OF THE SENATE .* Day /", "", $unformatted_text);

      // This section attempts to format the vote text for easy parsing
      // Use # as a way to easily split the string
      $unformatted_text = substr($unformatted_text, strpos($unformatted_text, ':') + 1);
      $unformatted_text = str_replace(', none', ', 0: none', $unformatted_text);
      $unformatted_text = str_replace('.', '', $unformatted_text);
      $unformatted_text = str_replace('Yeas', '#Yeas', $unformatted_text);
      $unformatted_text = str_replace('Nays', '#Nays', $unformatted_text);
      $unformatted_text = str_replace('Present', '#Present', $unformatted_text);
      $unformatted_text = str_replace('Absent', '#Absent', $unformatted_text);

      foreach(explode('#', $unformatted_text) as $position) {
        if (!empty($position)) {
          $pos_array = explode(':', $position);
          $pos_vote_count = explode(',', $pos_array[0]);
          $pos_names = explode(' ', $pos_array[1]);
          $pos_count = trim($pos_vote_count[1]);
          if ($pos_count != '0') {
            $cleaned_names = array();
            for ($i=0; $i < count($pos_names); $i += 1) {
              if (preg_match("/\d/",$pos_names[$i])==1){
                $i += 6;
              }
              elseif (!empty($pos_names[$i])) {
                array_push($cleaned_names, $pos_names[$i]);
              }
            }
            $votes[$pos_vote_count[0]] = $cleaned_names;
          }
        }
      }
      dpr($votes);

      $data['actions'][] = $action;
    }
  }
}

/**
 * Convert pdf to plain text.
 *
 * @param $content
 *   PDF document
 * @return
 *   Plain text of pdf with the position of the string set to beginning.
 */
function legislature_pdf2string($content) {
  $searchstart = 'stream';
  $searchend = 'endstream';
  $pdfText = '';
  $pos = 0;
  $pos2 = 0;
  $startpos = 0;

  while ($pos !== FALSE && $pos2 !== FALSE) {
    $pos = strpos($content, $searchstart, $startpos);
    $pos2 = strpos($content, $searchend, $startpos + 1);

    if ($pos !== FALSE && $pos2 !== FALSE){
      if ($content[$pos] == 0x0d && $content[$pos + 1] == 0x0a) {
        $pos += 2;
      }
      elseif ($content[$pos] == 0x0a) {
        $pos += 1;
      }

      if ($content[$pos2 - 2] == 0x0d && $content[$pos2 - 1] == 0x0a) {
        $pos2 -= 2;
      }
      elseif ($content[$pos2 - 1] == 0x0a) {
        $pos2 -= 1;
      }

      $textsection = substr($content, $pos + strlen($searchstart) + 2, $pos2 - $pos - strlen($searchstart) - 1);
      $data = @gzuncompress($textsection);
      $pdfText .= legislature_pdf_extract_text($data);
      $startpos = $pos2 + strlen($searchend) - 1;
    }
  }

  return preg_replace('/(\s)+/', ' ', $pdfText);
}

/**
 * Used for pdf conversion.
 *
 * @param $psData
 *   Uncompressed data.
 * @return
 *   Plain text of pdf.
 */
function legislature_pdf_extract_text($psData){
  if (!is_string($psData)) {
    return '';
  }

  $text = '';

  // Handle brackets in the text stream that could be mistaken for the end of a
  // text field. I'm sure you can do this as part of the regular expression,
  // but my skills aren't good enough yet.
  $psData = str_replace('\)', '##ENDBRACKET##', $psData);
  $psData = str_replace('\]', '##ENDSBRACKET##', $psData);

  preg_match_all('/(T[wdcm*])[\s]*(\[([^\]]*)\]|\(([^\)]*)\))[\s]*Tj/si', $psData, $matches);
  for ($i = 0; $i < sizeof($matches[0]); $i += 1) {
    if ($matches[3][$i] != '') {
      // Run another match over the contents.
      preg_match_all('/\(([^)]*)\)/si', $matches[3][$i], $subMatches);
      foreach ($subMatches[1] as $subMatch) {
        $text .= $subMatch;
      }
    } else if ($matches[4][$i] != '') {
      $text .= ($matches[1][$i] == 'Tc' ? ' ' : '') . $matches[4][$i];
    }
  }

  // Translate special characters and put back brackets.
  $trans = array(
    '...'                => '…',
    '\205'                => '…',
    '\221'                => chr(145),
    '\222'                => chr(146),
    '\223'                => chr(147),
    '\224'                => chr(148),
    '\226'                => '-',
    '\267'                => '•',
    '\('                => '(',
    '\['                => '[',
    '##ENDBRACKET##'    => ')',
    '##ENDSBRACKET##'    => ']',
    chr(133)            => '-',
    chr(141)            => chr(147),
    chr(142)            => chr(148),
    chr(143)            => chr(145),
    chr(144)            => chr(146),
  );
  return strtr($text, $trans);
}
