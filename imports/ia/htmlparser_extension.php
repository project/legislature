<?php

include drupal_get_path('module', 'legislature') .'/htmlparser.inc';

class LegislatureIaHtmlParser extends HtmlParser {
  /**
   * Returns an array of strings from a <tr> tag where each string
   * represents the inner text of of each <td>
   */
  function get_tr($parser) {
    $list = array();      
    while($parser->parse()) {
      if ($parser->is_tag($parser,"tr") && $parser->is_end_tag($parser)) {
        break;
      }
      if ($parser->is_tag($parser,"td") && $parser->is_start_tag($parser)) {
        array_push($list, $parser->get_inner_text($parser, "td"));
      }
    }
    return $list;
  }

  /**
   * Returns the inner text of a given tag. Parses an html page, checks for the
   * given tag and concats the inner text of the tag to a string, creating the
   * inner tag text Used to return the text between tags.
   */
  function get_inner_text($parser, $tag) {
    $text = "";     
    while($parser->parse()) {
      if ($parser->is_tag($parser, $tag) && $parser->is_end_tag($parser)) {
        break;
      }
      if ($parser->iNodeValue != NULL) {
        $text .= $parser->iNodeValue;
      }
    }

    return trim($text);
  }

  /**
   * Returns true or false if the current iNodeType is 2, which represents an
   * end tag.
   */
  function is_tag($parser, $tag){
    if (strcmp($parser->iNodeName, $tag) == 0) {
      return true;
    } 
    else {
      return false;
    }
  }

  /**
   * Returns true or false if the current iNodeType is 1, which represents the
   * start of a tag.
   */
  function is_start_tag($parser) {
    if ($parser->iNodeType == 1) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Returns true or false if the current iNodeType is 2, which represents an
   * end tag.
   */
  function is_end_tag($parser) {
    if ($parser->iNodeType == 2) {
      return true;
    }
    else {
      return false;
    }
  }
}
