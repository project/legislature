<?php

/**
 * @file
 * Imports all the data associated Iowa Senate and House members.
 */

/**
 * Imports all politicians' information to a database
 */
function legislature_ia_politicians() {
  set_time_limit(0);

  $session_years = legislature_session_years();
  $elected_jobs = array();

  foreach (legislature_get_enabled_sessions('ia') as $session) {
    // Retrieving all the politicians' information based on the GA number
    $assembly = 'http://www3.legis.state.ia.us/ga/legislators.do?ga='. $session;
    $get_request = drupal_http_request($assembly);
    $politicians = legislature_ia_politicians_data($get_request->data);

    foreach ($politicians as $politician_entry) {
      $elected_job = $politician_entry['elected_job'];
      $elected_job['start'] = gmmktime(0, 0, 0, 1, 1, $session_years['ia'][$session]['start']);
      $elected_job['end'] = gmmktime(0, 0, 0, 1, -1, $session_years['ia'][$session]['end'] + 1);
      unset($politician_entry['elected_job']);

      $politician_id = legislature_politician($politician_entry, 'legis');
      $elected_jobs[$politician_id][] = $elected_job;
    }

    foreach ($elected_jobs as $politician_id => $job_list) {
      legislature_save_elected_jobs('ia', $politician_id, $job_list);
    }
  }
}

/**
 * The main method that parses a roster page and personal information page.
 *
 * @param $html_page
 *   The roster page as raw html
 *
 * @return
 *   An array structure of all politicians and their personal information
 */
function legislature_ia_politicians_data($html_page) {
  include drupal_get_path('module', 'legislature') .'/imports/ia/htmlparser_extension.php';

  $parser = new LegislatureIaHtmlParser($html_page);
  $politicians = array();
  while ($parser->parse()) {
    if ($parser->is_tag($parser,"table") && $parser->is_start_tag($parser) && $parser->iNodeAttributes["class"]=="legistable") {
      while ($parser->parse()) {
        if ($parser->is_tag($parser,"tr") && $parser->is_start_tag($parser)) {
          $politician_entry = legislature_ia_politicians_data_from_tr($parser);
          if (!is_null($politician_entry)) {
            array_push($politicians, $politician_entry);
          }
        }
        if ($parser->is_tag($parser,"table") && $parser->is_end_tag($parser)) {
          break;
        }
      }
    }
  }
  return $politicians;
}

/**
 * Parses through a GA roster page and retrieves each politician's information.
 * Also generates a single element of the politician's array structure
 *
 * i.e. Sample element of structure
 *   last_name        => Angelo
 *   full_name        => Jeff Angelo
 *   roster_name      => Senator Jeff Angelo
 *   party            => Republican
 *   email            => email_address
 *   fax              => fax #
 *   phone            => phone #
 *
 * @params
 *   An HtmlParser object
 *
 * @return
 *   A single element of the politicians array structure
 */
function legislature_ia_politicians_data_from_tr($parser) {
  static $offices = array(
    'Senator' => 'Senate',
    'Representative' => 'House',
  );

  $list = array();
  $get_link = True;
  while ($parser->parse()) {
    if ($parser->is_tag($parser,"tr") && $parser->is_end_tag($parser)) {
      break;
    }
    if ($parser->is_tag($parser,"th") && $parser->is_start_tag($parser)) {
      return NULL;
    }
    if ($parser->is_tag($parser,"td") && $parser->is_start_tag($parser)) {
      $txt_ind = $parser->iHtmlTextIndex;
      array_push($list, $parser->get_inner_text($parser,"td",False));
      if ($get_link) {
        $parser->setTextIndex($txt_ind);
        while ($parser->parse()) {
          if ($parser->is_tag($parser,"td") && $parser->is_end_tag($parser)) {
            break;
          }
          if ($parser->is_tag($parser,"a") && $parser->is_start_tag($parser)) {
            $get_link = False;
            array_push($list, $parser->iNodeAttributes["href"]);
          }
        }
      }
    }
  }

  $district_matches = array();
  preg_match("/\d+$/", $list[2], $district_matches);
  $district_name = $offices[$title] .' Disrict '. $district_matches[0] ." (". $list[4] .")";
  $district_number = $district_matches[0];

  $last_name_matches = array();
  preg_match("/( ([^ ]+)$)|( ([^ ]+),)/",$list[0], $last_name_matches);

  $personal_page = "http://www3.legis.state.ia.us/ga/" . $list[1];
  $personal_info = legislature_ia_politicians_personal_page($personal_page);
  list($title, $full_name) = explode(' ', $list[0], 2);

  return array(
    'last_name'       => $last_name_matches[count($last_name_matches)-1],
    'full_name'       => $full_name,
    'roster_name'     => $list[0],
    'party'           => $list[3],
    'email'           => $personal_info['email'],
    'fax'             => $personal_info['fax'],
    'phone'           => $personal_info['phone'],
    'legis_id'        => preg_replace('/^.*[;?&]id=([0-9]*)[;?&].*$/','\1', $list[1]),
    'elected_job' => array(
      'district_id' => legislature_district('ia', $offices[$title], 'IA', $district_number, $district_name),
      'office_id' => legislature_get_office_id('IA', $offices[$title]),
      'is_candidate' => FALSE,
    ),
  );
}

/**
 * Parses a personal information page of a politician.
 *
 * @param $personal_page
 *   The personal information page of a politician as raw html.
 *
 * @return
 *   An array structure of a politician's personal/contact information.
 */
function legislature_ia_politicians_personal_page($personal_page) {
  $personal_info = array(
    "email"  =>  NULL,
    "fax"    =>  NULL,
    "phone"  =>  NULL,
  );

  $personal_info_matches = array();
  $get_request = drupal_http_request($personal_page);
  $raw_politician_personal_page = $get_request->data;
  $politician_page = preg_replace("/<br\/>/","**BR**",$raw_politician_personal_page);

  preg_match("/E-mail: ([^\*\*BR\*\*]+)\*\*BR\*\*/", $politician_page, $personal_info_matches);
  $personal_info["email"] = $personal_info_matches[count($personal_info_matches)-1];

  preg_match("/Fax: ([^\*\*BR\*\*]+)\*\*BR\*\*/", $politician_page, $personal_info_matches);
  $personal_info["fax"] = $personal_info_matches[count($personal_info_matches)-1];

  preg_match("/Telephone: ([^\*\*BR\*\*]+)\*\*BR\*\*/", $politician_page, $personal_info_matches);
  $personal_info["phone"] = $personal_info_matches[count($personal_info_matches)-1];

  return $personal_info;
}
