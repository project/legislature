<?php
// $Id:

/**
 * A list of California Legislature tables mapped to their data files.
 */
function legislature_ca_table_file_mapping() {
  //$table_file = array(
    //'legislature_ca_bill_analysis' => 'BILL_ANALYSIS_TBL.dat',
  //);
  //return $table_file;
  $table_file = array(
    'legislature_ca_bill_analysis' => 'BILL_ANALYSIS_TBL.dat',
    'legislature_ca_bill_detail_vote' => 'BILL_DETAIL_VOTE_TBL.dat', 
    'legislature_ca_bill_history' => 'BILL_HISTORY_TBL.dat', 
    'legislature_ca_bill_motion' => 'BILL_MOTION_TBL.dat', 
    'legislature_ca_bill_summary_vote' => 'BILL_SUMMARY_VOTE_TBL.dat', 
    'legislature_ca_bill' => 'BILL_TBL.dat', 
    'legislature_ca_bill_version_authors' => 'BILL_VERSION_AUTHORS_TBL.dat', 
    'legislature_ca_bill_version' => 'BILL_VERSION_TBL.dat', 
    'legislature_ca_codes' => 'CODES_TBL.dat', 
    'legislature_ca_committee_hearing' => 'COMMITTEE_HEARING_TBL.dat', 
    'legislature_ca_daily_file' => 'DAILY_FILE_TBL.dat', 
    'legislature_ca_legislator' => 'LEGISLATOR_TBL.dat', 
    'legislature_ca_location_code' => 'LOCATION_CODE_TBL.dat',          
  );
    // The following large data files are not used, no need to load 
    //'legislature_ca_law_section' => 'LAW_SECTION_TBL.dat', 
    //'legislature_ca_law_toc_sections' => 'LAW_TOC_SECTIONS_TBL.dat', 
    //'legislature_ca_law_toc' => 'LAW_TOC_TBL.dat', 
  return $table_file;
}

/**
 * A list of California Legislature tables mapped to their fields.
 */
function legislature_ca_table_fields_mapping() {
  $file_table = array(
    'legislature_ca_bill_analysis' => array('analysis_id','bill_id', 'house', 'analysis_type', 'committee_code', 'committee_name', 'amendment_author', 'analysis_date', 'amendment_date', 'page_num', 'source_doc', 'released_floor', 'active_flg', 'trans_uid', 'trans_update',),
    'legislature_ca_bill_detail_vote' => array('bill_id', 'location_code', 'legislator_name', 'vote_date_time', 'vote_date_seq', 'vote_code', 'motion_id', 'trans_uid', 'trans_update',), 
    'legislature_ca_bill_history' => array('bill_id', 'bill_history_id', 'action_date', 'action', 'trans_uid', 'trans_update_dt', 'action_sequence', 'action_code', 'action_status', 'primary_location', 'secondary_location', 'ternary_location', 'end_status',), 
    'legislature_ca_bill_motion' => array('motion_id', 'motion_text', 'trans_uid', 'trans_update',), 
    'legislature_ca_bill_summary_vote' => array('bill_id', 'location_code', 'vote_date_time', 'vote_date_seq', 'motion_id', 'ayes', 'noes', 'abstain', 'vote_result', 'trans_uid', 'trans_update',), 
    'legislature_ca_bill' => array('bill_id', 'session_year', 'session_num', 'measure_type', 'measure_num', 'measure_state', 'chapter_year', 'chapter_type', 'chapter_session_num', 'chapter_num', 'latest_bill_version_id', 'active_flg', 'trans_uid', 'trans_update', 'current_location', 'current_secondary_loc', 'current_house', 'current_status',), 
    'legislature_ca_bill_version_authors' => array('bill_version_id', 'type', 'house', 'name', 'contribution', 'committee_members', 'active_flg', 'trans_uid', 'trans_update', 'primary_author_flg',), 
    'legislature_ca_bill_version' => array('bill_version_id', 'bill_id', 'version_num', 'bill_version_action_date', 'bill_version_action', 'request_num', 'subject', 'vote_required', 'appropriation', 'fiscal_committee', 'local_program', 'substantive_changes', 'urgency', 'taxlevy', 'bill_xml', 'active_flg', 'trans_uid', 'trans_update',), 
    'legislature_ca_codes' => array('code', 'title',), 
    'legislature_ca_committee_hearing' => array('bill_id', 'committee_type', 'committee_nr', 'hearing_date', 'location_code', 'trans_uid', 'trans_update_date',), 
    'legislature_ca_daily_file' => array('bill_id', 'location_code', 'consent_calendar_code', 'file_location', 'publication_date',), 
    'legislature_ca_law_section' => array('id', 'law_code', 'section_num', 'op_statues', 'op_chapter', 'op_section', 'effective_date', 'law_section_version_id', 'division', 'title', 'part', 'chapter', 'article', 'history', 'content_xml', 'active_flg', 'trans_uid', 'trans_update',), 
    'legislature_ca_law_toc_sections' => array('id', 'law_code', 'node_treepath', 'section_num', 'section_order', 'title', 'op_statues', 'op_chapter', 'op_section', 'trans_uid', 'trans_update', 'law_section_version_id',), 
    'legislature_ca_law_toc' => array('law_code', 'division', 'title', 'part', 'chapter', 'article', 'heading', 'active_flg', 'trans_uid', 'trans_update', 'node_sequence', 'node_level', 'node_position', 'node_treepath', 'contains_law_sections', 'history_note', 'op_statues', 'op_chapter', 'op_section',), 
    'legislature_ca_legislator' => array('district', 'session_year', 'legislator_name', 'house_type', 'author_name', 'first_name', 'last_name', 'middle_initial', 'name_suffix', 'name_title', 'web_name_title', 'party', 'active_flg', 'trans_uid', 'trans_update',), 
    'legislature_ca_location_code' => array('session_year', 'location_code', 'location_type', 'consent_calendar_code', 'description', 'long_description', 'active_flg', 'trans_uid', 'trans_update',),          
  );
  return $file_table;
}
