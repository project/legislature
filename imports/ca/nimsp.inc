<?php
// $Id:

/**
 * Load the downloaded NIMSP data to the legislature database tables.
 *
 */
function legislature_ca_move_nimsp() {
  
  // *** legislature_politician ***
  
  // Select NIMSP records for loading politician data
  $result = db_query("SELECT RecipientID, Cand_Name, Cand_Prefix_Name, Cand_First_Name, Cand_Middle_Name, 
                      Cand_Last_Name, Cand_Suffix_Name, Cand_Nick_Name, Cand_Party, Gender FROM {nimsp_ca} 
                      WHERE Cand_Name != '' AND YearCode IN ('2003', '2004','2006','2008','2009') 
                      GROUP BY RecipientID");
  
  // Cycle through records
  $count = 0;
  while ($row_nimsp_politician = db_fetch_object($result)) {
    
    // Does legislator already exist? Check first, last, and middles names from same jurisdiction.
    $politician_id = db_result(db_query("SELECT p.politician_id FROM {legislature_politician} p 
                                         INNER JOIN {legislature_elected_job} j ON p.politician_id = j.politician_id 
                                         INNER JOIN {legislature_office} o ON j.office_id = o.office_id 
                                         WHERE p.last_name = '%s' AND p.first_name LIKE '%%%s%%' 
                                         AND (p.middle_name = '' OR '%s' = '' OR p.middle_name = '%s') 
                                         AND j.is_most_recent = 1 and LOWER(o.jurisdiction) = 'ca'", 
                                         ucwords(strtolower($row_nimsp_politician->Cand_Last_Name)), 
                                         ucwords(strtolower($row_nimsp_politician->Cand_First_Name)),
                                         ucwords(strtolower($row_nimsp_politician->Cand_Middle_Name)),
                                         ucwords(strtolower($row_nimsp_politician->Cand_Middle_Name))));
                                         
    // Get full name                                         
    $full_name = ucwords(strtolower($row_nimsp_politician->Cand_First_Name)) . ' ';
    $full_name .= ($row_nimsp_politician->Cand_Middle_Name != '') ? ucwords(strtolower($row_nimsp_politician->Cand_Middle_Name)) . ' ' : '';
    $full_name .= ucwords(strtolower($row_nimsp_politician->Cand_Last_Name));                   
                                         
    $row_legislature_politician = new stdClass();
    $row_legislature_politician->full_name = $full_name;
    $row_legislature_politician->roster_name = ucwords(strtolower($row_nimsp_politician->Cand_Name));
    $row_legislature_politician->imsp_name = $row_nimsp_politician->Cand_Name;
    $row_legislature_politician->first_name = ucwords(strtolower($row_nimsp_politician->Cand_First_Name));
    $row_legislature_politician->last_name = ucwords(strtolower($row_nimsp_politician->Cand_Last_Name));
    $row_legislature_politician->middle_name = ucwords(strtolower($row_nimsp_politician->Cand_Middle_Name));
    $row_legislature_politician->nick_name = ucwords(strtolower($row_nimsp_politician->Cand_Nick_Name));
    $row_legislature_politician->name_prefix = $row_nimsp_politician->Cand_Prefix_Name;
    $row_legislature_politician->name_modifier = $row_nimsp_politician->Cand_Suffix_Name;
    $row_legislature_politician->party = ucwords(strtolower($row_nimsp_politician->Cand_Party));
    $gender = '';
    switch ($row_nimsp_politician->Gender) {
      case 'M':
        $gender = 'Male';
      case 'F':
        $gender = 'Female';
    }
    $row_legislature_politician->Gender = $gender;
    $row_legislature_politician->legis_id = '';
    $row_legislature_politician->congresspedia_url = '';
    $row_legislature_politician->fec_id = '';    
    
    // Exists?
    if (!$politician_id) {
      // Insert full record
      drupal_write_record('legislature_politician', $row_legislature_politician);
      $politician_id = db_last_insert_id('{legislature_politician}', 'politician_id');
      watchdog('legislature', '%politician (%politician_id) NOT FOUND, ADDED.', array('%politician' => $row_legislature_politician->full_name, '%politician_id' => $politician_id), WATCHDOG_NOTICE);
    } else {
      // Only update IMSP name field
      
      watchdog('legislature', '%politician (%politician_id) FOUND.', array('%politician' => $row_legislature_politician->full_name, '%politician_id' => $politician_id), WATCHDOG_NOTICE);
    }
    
    $count++;
    if ($count > 50) {
      break;
    }
    
  }
  
  return;

  // *** map_contribution ***
  
  // Select NIMSP records for loading contribution data
  $result = db_query("SELECT n.ContributionID, n.StateCode, n.YearCode, n.Amount, n.Original_Amount, n.Date, n.CatCode, n.Committee_Name, 
                      n.Cand_Name, n.Recipient_Party, n.Recipient_Office, n.District, n.RecipientID, n.Contributor, n.New_Contributor, 
                      n.First, n.Last, n.City, n.State, n.ZipCode, n.Zip5, n.PAC_Name, n.Type, n.Occupation, n.New_Employer, n.Employer, 
                      n.Zip4, n.Last, p.politician_id
                      FROM {nimsp_ca} n INNER JOIN {legislature_politician} p ON p.imsp_name = n.Cand_Name 
                      WHERE Recipient_Type IN ('C') AND YearCode IN ('2003', '2004', '2006', '2008', '2009') 
                      AND Web_Suppress = '0' AND Type IN ('DIR','I','SMALL','REF','RET')");
  
  // Cycle through records
  $count = 0;
  while ($row_nimsp_map_contribution = db_fetch_object($result)) {
    
    // Does contribution already exist?
    $row_existing = db_fetch_object(db_query("SELECT recordid, statecode, yearcode FROM {map_contribution}  
                                              WHERE recordid = '%s' AND statecode LIKE '%s%' AND yearcode = '%s'", 
                                              $row_nimsp_map_contribution->ContributionID, 
                                              $row_nimsp_map_contribution->StateCode,
                                              $row_nimsp_map_contribution->YearCode));            
    
    $row_map_contribution = new stdClass();
    $row_map_contribution->recordid = $row_nimsp_map_contribution->ContributionID;
    $row_map_contribution->statecode = $row_nimsp_map_contribution->StateCode;
    $row_map_contribution->yearcode = $row_nimsp_map_contribution->YearCode;
    $row_map_contribution->contrib = $row_nimsp_map_contribution->Amount;
    $row_map_contribution->date = $row_nimsp_map_contribution->Amount;
    $row_map_contribution->business_id = $row_nimsp_map_contribution->CatCode;
    $cand_types = array("C", "S");
    $committee_types = array("B", "I", "P", "X");
    $row_map_contribution->recipient = "";
    if (in_array($row_nimsp_map_contribution->ContributionID, $cand_types)) {
      $row_map_contribution->recipient = $row_nimsp_map_contribution->Cand_Name;
    } else if (in_array($row_nimsp_map_contribution->ContributionID, $committee_types)) {
      $row_map_contribution->recipient = $row_nimsp_map_contribution->Committee_Name;
    }
    $row_map_contribution->party = substr($row_nimsp_map_contribution->Recipient_Party, 0, 1);
    $row_map_contribution->office = $row_nimsp_map_contribution->Recipient_Office;
    $row_map_contribution->district = $row_nimsp_map_contribution->District;
    $row_map_contribution->recipient_id = $row_nimsp_map_contribution->RecipientID;
    $row_map_contribution->contributor_name = $row_nimsp_map_contribution->New_Contributor;
    // = $row_nimsp_map_contribution->First;
    // = $row_nimsp_map_contribution->Last;
    $row_map_contribution->city = $row_nimsp_map_contribution->City;
    $row_map_contribution->state = $row_nimsp_map_contribution->State;
    $zip_code = $row_nimsp_map_contribution->ZipCode;
    if ($zip_code == '' || $zip_code == '0') {
      $zip_code = $row_nimsp_map_contribution->Zip5;
    }
    $row_map_contribution->politician_id = $row_nimsp_map_contribution->politician_id;
    $individual_name = $row_nimsp_map_contribution->First;
    if ($row_nimsp_map_contribution->First != '') {
       $individual_name = $row_nimsp_map_contribution->First . ', ' . $individual_name;
    }
    $row_map_contribution->individual_name = $individual_name;
    //$row_map_contribution->pac_id = ;
    // = $row_nimsp_map_contribution->PAC_Name;
    //$row_map_contribution->date_int = ;
    //$row_map_contribution->street_address = ;
    //$row_map_contribution->address_id = ;
    //$row_map_contribution->source_id = ;
    //$row_map_contribution->contribution_id = ;
    $row_map_contribution->type = $row_nimsp_map_contribution->Type;
    //$row_map_contribution->contrib_int = ;
    $row_map_contribution->is_individual = ($row_nimsp_map_contribution->Last != '') ? 1 : 0;
    //$row_map_contribution->care_of = ;
    //$row_map_contribution->crp_presidential = ;
    $row_map_contribution->cmte_id = "NEW";
    // = $row_nimsp_map_contribution->ICO;
    // = $row_nimsp_map_contribution->Contributor_Owner;
    
    // Only insert if doesn't exist
    if (!$row_existing) {
      drupal_write_record('map_contribution', $row_map_contribution);
      watchdog('legislature', 'map_contribution record added: %recordid', array('%recordid' => $row_map_contribution->recordid), WATCHDOG_NOTICE);
    } else {
      $keys = array('recordid', 'statecode', 'yearcode');
      drupal_write_record('map_contribution', $row_map_contribution, $keys);
      watchdog('legislature', 'map_contribution record updated: %recordid', array('%recordid' => $row_map_contribution->recordid), WATCHDOG_NOTICE);
    }
    
    $count++;
    if ($count > 50) {
      break;
    }
    
  }
  
  return;
    
}