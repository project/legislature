<?php
// $Id:

/**
 * Load the downloaded California Legislature data.
 *
 * @param $session
 *   A session name
 * @param $day
 *   A day of the week (example: 'Sun')
 */
function legislature_ca_load_state_data($session = '2009', $day = '') {
  legislature_ca_load_files($session, $day, 'all');
  $clear = ($day == 'Sun') ? true : false;
  $suffix = ($day == 'Sun') ? $session : $day;
  legislature_ca_move_legislators($session, $clear);
  legislature_ca_move_committees($session, $clear);
  legislature_ca_move_bills($session, $clear, $suffix);
  legislature_ca_move_actions($session, $clear, $suffix);
}

/**
 * Load the downloaded California Legislature data from the data files to the database tables.
 *
 * @param $session
 *   A session name
 * @param $day
 *   A day of the week (example: 'Sun')
 * @param $table
 *   A table name or 'all' to load all tables
 * @return
 *   Number records written.
 */
function legislature_ca_load_files($session = '2009', $day = '', $table = 'all') {
  
  // Data stored in day-based subdirectories (Sun, Mon, Tue...)
  $dir = variable_get('legislature_ca_data_directory', '') . '/' . $day;
  
  // Include state table descriptions
  include_once './' . drupal_get_path('module', 'legislature') .'/imports/ca/leginfo_mapping.inc';

  // The default is to load all tables
  $table_file = legislature_ca_table_file_mapping(); 
  $table_columns = legislature_ca_table_fields_mapping();
  
  // Cycle through all tables and delete data
  foreach ($table_file as $key => $value) {
    // Exception: don't delete location_code table if an incremental update (all days but Sunday are incremental)
    if($key == 'legislature_ca_location_code' && $day != 'Sun') {
      continue;
    }
    db_query("DELETE FROM {" . $key . "}");
    watchdog('legislature', '%key table deleted.', array('%key' => $key, '%table_name' => $table), WATCHDOG_NOTICE);
  }
  // If doing a full load (Sunday), delete temp mapping table data (keep around for incremental updates)
  if ($day == 'Sun') {
    db_query("DELETE FROM {legislature_ca_temp}");   
    watchdog('legislature', '%table table deleted.', array('%table' => 'legislature_ca_temp'), WATCHDOG_NOTICE);
  }
  
  // If table name passed in, load only it
  if (array_key_exists($table, $table_file)) {
    $table_file = array($table => $table_file[$table]); // single table
  }
  
  $records_written = 0;
    
  // Cycle through each table to be loaded
  foreach ($table_file as $table => $file) {
    
    // Get data file
    $file_name = $dir . '/' . $file;
    if (!is_readable($file_name)) {
      continue;
    }
    $fh = fopen($file_name, 'rb');
    watchdog('legislature', 'File opened: %file', array('%file' => $file_name), WATCHDOG_NOTICE);
    
    // Get db columns
    $columns = $table_columns[$table];
    
    $line_number = 0;
    $column_count = count($columns);
    
    // Cycle through data file lines
    while (($line = fgetcsv($fh, 0, "\t", "`")) !== FALSE) {
      $line_number += 1;
      if (count($line) !== $column_count) {
        watchdog('legislature', 'Line @line_number of %file has the wrong number of columns, @column1 vs. @column2, %file_name, %table_name, %line1.', array('%file' => $file_name, '@line_number' => $line_number, '@column1' => count($line), '@column2' => $column_count, '%file_name' => $file, '%table_name' => $table_name, '%line1' => $line[0]), WATCHDOG_WARNING);
      }
      else {
        $line = nullstrings_to_empty($line);
        $line = array_combine($columns, $line);
        $records_written += (drupal_write_record($table, $line)) ? 1 : 0;
      }
    }
    watchdog('legislature', '%file_name loaded into %table_name.', array('%file_name' => $file, '%table_name' => $table), WATCHDOG_NOTICE);
    fclose($fh);
  } 
  
  watchdog('legislature', '%session/%day/%records_written records written.', array('%session' => $session, '%day' => $day, '%records_written' => $records_written), WATCHDOG_NOTICE);
  return $records_written;
  
}

/**
 * Move California Legislature legislator data into politician and elected job tables. 
 * Affected table(s): legislature_politician, legislature_elected_job, legislature_elected_job_session
 */
function legislature_ca_move_legislators($session = '2009', $clear = false) {
  
  // Select all legislator entries in state table
  $result = db_query("SELECT * FROM {legislature_ca_legislator}");
  
  // Cycle through legislator entries
  while ($row_legislature_ca_legislator = db_fetch_object($result)) {
    
    // If entry has a vacancy, do not move
    if (strstr($row_legislature_ca_legislator->legislator_name, 'Vacancy')) {
      continue;
    }
    
    // *** legislature_politician table ***
    
    // Does legislator already exist? Check first, last, and middles names from same jurisdiction.
    $politician_id = db_result(db_query("SELECT p.politician_id FROM {legislature_politician} p 
                                         INNER JOIN {legislature_elected_job} j ON p.politician_id = j.politician_id 
                                         INNER JOIN {legislature_office} o ON j.office_id = o.office_id 
                                         WHERE p.last_name = '%s' AND p.first_name LIKE '%%%s%%' 
                                         AND (p.middle_name = '' OR '%s' = '' OR p.middle_name = '%s') 
                                         AND j.is_most_recent = 1 and LOWER(o.jurisdiction) = 'ca'", 
                                         $row_legislature_ca_legislator->last_name, $row_legislature_ca_legislator->first_name,
                                         $row_legislature_ca_legislator->middle_initial, $row_legislature_ca_legislator->middle_initial));
    // Get full name                                         
    $full_name = $row_legislature_ca_legislator->first_name . ' ';
    $full_name .= ($row_legislature_ca_legislator->middle_initial != '') ? $row_legislature_ca_legislator->middle_initial . ' ' : '';
    $full_name .= $row_legislature_ca_legislator->last_name;

    // Get party
    // Map state abbrevs to legislature database abbrevs
    $parties = array(
      'DEM' => 'Democratic',
      'REP' => 'Republican',
    );
    $party = (isset($parties[$row_legislature_ca_legislator->party])) ? $parties[$row_legislature_ca_legislator->party] : $row_legislature_ca_legislator->party;
    
                                         
    $row_legislature_politician = new stdClass();
    $row_legislature_politician->full_name = $full_name;
    $row_legislature_politician->roster_name = $row_legislature_ca_legislator->legislator_name;
    $row_legislature_politician->first_name = $row_legislature_ca_legislator->first_name;
    $row_legislature_politician->last_name = $row_legislature_ca_legislator->last_name;
    $row_legislature_politician->middle_name = $row_legislature_ca_legislator->middle_initial;
    $row_legislature_politician->name_modifier = $row_legislature_ca_legislator->name_suffix;
    $row_legislature_politician->party = $party;
    $row_legislature_politician->roster_name = $row_legislature_ca_legislator->legislator_name;
    
    if ($politician_id) {
      // already exists so update record
      $row_legislature_politician->politician_id = $politician_id;
      drupal_write_record('legislature_politician', $row_legislature_politician, 'politician_id');
    } else {
      // does not exist so insert new record
      drupal_write_record('legislature_politician', $row_legislature_politician);
      $politician_id = db_last_insert_id('{legislature_politician}', 'politician_id');
    }      

    // Map author_name to politician_id
    $row_legislature_ca_temp = new stdClass();
    $row_legislature_ca_temp->ca_name = 'author_name';
    $row_legislature_ca_temp->ca_value = $row_legislature_ca_legislator->author_name;
    $row_legislature_ca_temp->leg_name = 'politician_id';
    $row_legislature_ca_temp->leg_value = $politician_id;
    drupal_write_record('legislature_ca_temp', $row_legislature_ca_temp);
    
    // *** legislature_elected_job table ***
    
    // Get session ID
    // Session name is (for example) 20092010 in legislature_ca_legislator
    $session_obj = legislature_get_session('ca', substr($row_legislature_ca_legislator->session_year, 0, 4));
    $session_id = $session_obj->session_id;
                                      
    // Get district ID
    // SD04 and AD45 in legislature_ca_legislator are CA-Senate-4 and CA-Assembly-45 in legislature_district
    $district_office = ($row_legislature_ca_legislator->house_type == 'S') ? 'Senate' : 'Assembly';
    $district_num = intval(substr($row_legislature_ca_legislator->district, 2));
    $district_id = legislature_get_district_id('ca', 'CA', $district_office, $district_num);

    // Get office ID
    $office_title = ($row_legislature_ca_legislator->house_type === 'S') ? 'Senate' : 'Assembly';
    $office_id = legislature_get_office_id('ca', $office_title);

    // Does elected job already exist?
    $elected_job_id = db_result(db_query("SELECT j.elected_job_id FROM {legislature_elected_job} j
                                             WHERE j.politician_id = %d AND j.office_id = %d AND j.district_id = %d", 
                                             $politician_id, $office_id, $district_id));
                                             
    // Get start and end times for elected job
    // TODO For now, set start to Jan. 1 of first year of session and end to Dec. 31 of last year of session
    $year_start = substr($row_legislature_ca_legislator->session_year, 0, 4);
    $elected_job_start = gmmktime(0, 0, 0, 1, 1, $year_start);
    $year_end = substr($row_legislature_ca_legislator->session_year, 4, 4);
    $elected_job_end = gmmktime(0, 0, 0, 12, 31, $year_end);
    
    // Set this elected job as 0 by default, then update most recent for legislator below
    $elected_job_most_recent = 0;
    
    // Set is_candidate to 0
    $elected_job_is_candidate = 0;    

    $row_legislature_elected_job = new stdClass();
    $row_legislature_elected_job->politician_id = $politician_id;
    $row_legislature_elected_job->office_id = $office_id;
    $row_legislature_elected_job->district_id = $district_id;
    $row_legislature_elected_job->start = $elected_job_start;
    $row_legislature_elected_job->end = $elected_job_end;
    $row_legislature_elected_job->is_most_recent = $elected_job_most_recent;
    $row_legislature_elected_job->is_candidate = $elected_job_is_candidate;

    if ($elected_job_id) {
      // already exists so update record
      $row_legislature_elected_job->elected_job_id = $elected_job_id;
      drupal_write_record('legislature_elected_job', $row_legislature_elected_job, 'elected_job_id');
    } else {
      // does not exist so insert new record
      drupal_write_record('legislature_elected_job', $row_legislature_elected_job);
      $elected_job_id = db_last_insert_id('{legislature_elected_job}', 'elected_job_id');
    }                                             
    
    // Mark is_most_recent for all elected job rows as 0
    $result_most_recent = db_query("UPDATE {legislature_elected_job} SET is_most_recent = 0 WHERE politician_id = %d", $politician_id);
    
    // Mark record with most recent start value as 1
    $max_start = db_result(db_query("SELECT MAX(start) FROM {legislature_elected_job} WHERE  politician_id = %d", $politician_id));
    $result_most_recent = db_query("UPDATE {legislature_elected_job} SET is_most_recent = 1 WHERE politician_id = %d AND start = %d", 
                       $politician_id, $max_start);
    
    // *** legislature_elected_job_session table ***
    
    // Delete existing record from table
    if ($clear = FALSE) {
      db_query("DELETE FROM {legislature_elected_job_session}
                WHERE elected_job_id = %d AND session_id = %d", 
                $elected_job_id, $session_id);
    }
                                                 
    // Insert record
    if (!$elected_job_id2) {
      // does not exist so insert new record
      $row_legislature_elected_job_session = new stdClass();
      $row_legislature_elected_job_session->elected_job_id = $elected_job_id;
      $row_legislature_elected_job_session->session_id = $session_id;    
      drupal_write_record('legislature_elected_job_session', $row_legislature_elected_job_session);
    }
        
    watchdog('legislature', '%politician (%politician_id) added.', array('%politician' => $full_name, '%politician_id' => $politician_id), WATCHDOG_NOTICE);

  }

}

/**
 * Move California Legislature location data into committee table. 
 * Affected table(s): legislature_committee
 */
function legislature_ca_move_committees($session = '2009', $clear = FALSE) {
    
  // Delete existing records from table
  if ($clear = TRUE) {
    $session_obj = legislature_get_session('ca', $session);
    db_query("DELETE FROM {legislature_committee} WHERE session_id = %d", $session_obj->session_id);
  }
  
  // Select all committee entries in state table
  $result = db_query("SELECT * FROM {legislature_ca_location_code} 
                      WHERE SUBSTR(location_code, 1, 2) = 'CS' OR SUBSTR(location_code, 1, 2) = 'CX'");
  
  // Cycle through location code entries
  while ($row_legislature_ca_location_code = db_fetch_object($result)) {
        
    // *** legislature_committee table ***
    
    // Get office ID (based on first two chars of location code)
    $prefix_mapping = array('CS' => 'Senate', 'CX' => 'Assembly');
    $office_title = $prefix_mapping[substr($row_legislature_ca_location_code->location_code, 0, 2)];
    $office_id = legislature_get_office_id('ca', $office_title);
            
    // Get session ID
    // Session name is (for example) 20092010 in legislature_ca_legislator
    $session_obj = legislature_get_session('ca', substr($row_legislature_ca_location_code->session_year, 0, 4));
    $session_id = $session_obj->session_id;
        
    // Does committee already exist?
    // TODO check by description or code? (If code, need to add to legislature_committee)
    $committee_id = db_result(db_query("SELECT c.committee_id FROM {legislature_committee} c 
                                        INNER JOIN {legislature_office} o ON c.office_id = o.office_id 
                                        INNER JOIN {legislature_session} s ON c.session_id = s.session_id 
                                        WHERE c.name = '%s' AND c.office_id = %d AND s.session_id = %d", 
                                        $row_legislature_ca_legislator->description, $office_id, $session_id));
                                             
    $row_legislature_committee = new stdClass();
    $row_legislature_committee->session_id = $session_id;
    $row_legislature_committee->office_id = $office_id;
    $row_legislature_committee->name = $row_legislature_ca_location_code->description;
    
    if ($committee_id) {
      // already exists so update record
      $row_legislature_committee->committee_id = $committee_id;
      drupal_write_record('legislature_committee', $row_legislature_committee, 'committee_id');
    } else {
      // does not exist so insert new record
      drupal_write_record('legislature_committee', $row_legislature_committee);
      $committee_id = db_last_insert_id('{legislature_committee}', 'committee_id');
    }         

    // Map location_code to committee_id
    $row_legislature_ca_temp = new stdClass();
    $row_legislature_ca_temp->ca_name = 'location_code';
    $row_legislature_ca_temp->ca_value = $row_legislature_ca_location_code->location_code;
    $row_legislature_ca_temp->leg_name = 'committee_id';
    $row_legislature_ca_temp->leg_value = $committee_id;
    drupal_write_record('legislature_ca_temp', $row_legislature_ca_temp);
    
    watchdog('legislature', 'Committee added: %description.', array('%description' => $row_legislature_ca_location_code->description), WATCHDOG_NOTICE);

  }

}

/**
 * Move California Legislature bill data into bill tables. 
 * Affected table(s): legislature_bill, legislature_bill_committee, legislature_bill_sponsor, map_bill
 */
function legislature_ca_move_bills($session = '2009', $clear = false, $suffix = '2009') {
  
  // Delete existing records from tables
  if ($clear = TRUE) {
    $session_obj = legislature_get_session('ca', $session);
    db_query("DELETE lb, lbc, lbs, mb
              FROM legislature_bill AS lb
              LEFT JOIN legislature_bill_committee AS lbc ON lb.bill_id = lbc.bill_id
              LEFT JOIN legislature_bill_sponsor AS lbs ON lb.bill_id = lbs.bill_id
              LEFT JOIN map_bill AS mb ON lb.bill_id = mb.bill_id
              WHERE lb.session_id = %d", $session_obj->session_id);
  }  
  
  // Select all bill entries in state table
  $result = db_query("SELECT * FROM {legislature_ca_bill}");
  
  // Cycle through bill entries
  while ($row_legislature_ca_bill = db_fetch_object($result)) {
        
    // *** legislature_bill table ***
    
    // Get latest action date from history table 
    $last_version = db_result(db_query("SELECT DATE(MAX(action_date)) FROM {legislature_ca_bill_history} 
                                        WHERE bill_id = '%s' GROUP BY bill_id;", $row_legislature_ca_bill->bill_id));
                
    // Get session ID
    // Session name is (for example) 20092010
    $session_obj = legislature_get_session('ca', substr($row_legislature_ca_bill->session_year, 0, 4));
    $session_id = $session_obj->session_id;

    $measure  = $row_legislature_ca_bill->measure_type;
    $measure .= ($row_legislature_ca_bill->session_num == 0) ? '' : ' ' . 'X' . $row_legislature_ca_bill->session_num;
    $measure .= ' ' . $row_legislature_ca_bill->measure_num;
    
    // Does bill already exist?
    $bill_id = db_result(db_query("SELECT b.bill_id FROM {legislature_bill} b 
                                         WHERE b.session_id = %d AND b.measure = '%s'", 
                                         $session_id, $measure));
                                             
    $row_legislature_bill = new stdClass();
    $row_legislature_bill->measure = $measure;
    $row_legislature_bill->session_id = $session_id;
    $row_legislature_bill->prefix = $row_legislature_ca_bill->measure_type;
    $row_legislature_bill->status = $row_legislature_ca_bill->current_status;
    $row_legislature_bill->last_version = $last_version;
    // Try to get latest version topic and desc from version table in this data set
    $row_legislature_ca_bill_version = db_fetch_object(db_query("SELECT * FROM {legislature_ca_bill_version} WHERE bill_version_id = '%s'", $row_legislature_ca_bill->latest_bill_version_id));
    if (isset($row_legislature_ca_bill_version->bill_xml)) {
      // If version is found in this data set, look up in bill_xml field file
      // (If not found--because this is an incremental update--the correct data should already exist in legislature_bill record)
      $dir = variable_get('legislature_ca_data_directory', '') . '/pubinfo_'.$suffix;
      $file = $dir . '/' . $row_legislature_ca_bill_version->bill_xml;    
      $text = text_from_file($file);
      // TODO Fix SimpleXML-based retrieval
      //$xml = new SimpleXMLElement($text);
      // Handle XML's namespace
      //$children = $xml->children('http:lc.ca.gov/legalservices/schemas/caml.1#');
      //$row_legislature_bill->topic = trim((string)$xml->Description->Title);
      //$row_legislature_bill->description = trim(strip_tags((string)$children->Description->DigestText->asXML()));
      preg_match('/(?<=<caml:Title>).+?(?=<\/caml:Title>)/', preg_replace("/[\n\r]/","",$text), $matches_topic);
      $row_legislature_bill->topic = trim(strip_tags($matches_topic[0]));
      preg_match('/(?<=<caml:DigestText>).+?(?=<\/caml:DigestText>)/', preg_replace("/[\n\r]/","",$text), $matches_desc);
      $row_legislature_bill->description = trim(strip_tags($matches_desc[0]));
    }
    
    if ($bill_id) {
      // already exists so update record
      $row_legislature_bill->bill_id = $bill_id;
      drupal_write_record('legislature_bill', $row_legislature_bill, 'bill_id');
      $temp_result = 'updated: ';
    } else {
      // does not exist so insert new record
      drupal_write_record('legislature_bill', $row_legislature_bill);
      $bill_id = db_last_insert_id('{legislature_bill}', 'bill_id');
      $temp_result = 'created: ';
    } 
                                                
    // Map state bill ID to leg bill ID
    $row_legislature_ca_temp = new stdClass();
    $row_legislature_ca_temp->ca_name = 'bill_id';
    $row_legislature_ca_temp->ca_value = $row_legislature_ca_bill->bill_id;
    $row_legislature_ca_temp->leg_name = 'bill_id';
    $row_legislature_ca_temp->leg_value = $bill_id;
    drupal_write_record('legislature_ca_temp', $row_legislature_ca_temp);
    
    watchdog('legislature', 'Bill %temp_result %bill_id.', array('%temp_result' => $temp_result, '%bill_id' => $bill_id), WATCHDOG_NOTICE);
    
    // *** legislature_bill_committee table ***
    
    $result_history = db_query("SELECT * FROM {legislature_ca_bill_history} 
                        WHERE bill_id = '%s' AND 
                        (SUBSTR(ternary_location, 1, 2) = 'CS' OR SUBSTR(ternary_location, 1, 2) = 'CX')",
                        $row_legislature_ca_bill->bill_id);
                        
    // Cycle through bill history entries
    while ($row_legislature_ca_bill_history = db_fetch_object($result_history)) {
           
      // Get committee_id (mapped to location_code)
      $committee_id = db_result(db_query("SELECT leg_value FROM {legislature_ca_temp} 
                                          WHERE ca_name = 'location_code' AND ca_value = '%s' AND leg_name = 'committee_id'", 
                                          $row_legislature_ca_bill_history->ternary_location));
                                          
      $row_legislature_bill_committee = new stdClass();
      $row_legislature_bill_committee->bill_id = $bill_id;
      $row_legislature_bill_committee->committee_id = $committee_id;
      $row_legislature_bill_committee->activity = $row_legislature_ca_bill_history->action;
      $row_legislature_bill_committee->activity_date = strtotime($row_legislature_ca_bill_history->action_date);

      drupal_write_record('legislature_bill_committee', $row_legislature_bill_committee);
            
      watchdog('legislature', 'Bill committee added: %committee_id.', array('%committee_id' => $committee_id), WATCHDOG_NOTICE);
    
    }
      
    // *** legislature_bill_sponsor table ***
    
    // Get authors for latest version
    $result_authors = db_query("SELECT * FROM {legislature_ca_bill_version_authors} WHERE bill_version_id = '%s'",
                               $row_legislature_ca_bill->latest_bill_version_id);
                        
    // Cycle through authors
    while ($row_legislature_ca_bill_version_authors = db_fetch_object($result_authors)) {
           
      // Get politician_id (mapped to author name)
      $politician_id = db_result(db_query("SELECT leg_value FROM {legislature_ca_temp} 
                                          WHERE ca_name = 'author_name' AND ca_value = '%s' AND leg_name = 'politician_id'", 
                                          $row_legislature_ca_bill_version_authors->name));
                                          
      // Get type ID based contribution value
      $sponsor_types = array('LEAD_AUTHOR' => 1, 'COAUTHOR' => 2, 'PRINCIPAL_COAUTHOR' => 3);
      $sponsor_type = $sponsor_types[$row_legislature_ca_bill_version_authors->contribution];
                                          
      $row_legislature_bill_sponsor = new stdClass();
      $row_legislature_bill_sponsor->bill_id = $bill_id;
      $row_legislature_bill_sponsor->politician_id = $politician_id;
      $row_legislature_bill_sponsor->type = $sponsor_type;

      drupal_write_record('legislature_bill_sponsor', $row_legislature_bill_sponsor);
            
      watchdog('legislature', 'Bill sponsor added: %politician_id.', array('%politician_id' => $politician_id), WATCHDOG_NOTICE);
      
    }                  
      
    // *** map_bill table ***
    
    // Get subject for latest bill version
    $bill_version_subject = db_result(db_query("SELECT subject FROM {legislature_ca_bill_version} WHERE bill_version_id = '%s'",
                                               $row_legislature_ca_bill->latest_bill_version_id));
                                                                                              
    $row_map_bill = new stdClass();
    $row_map_bill->bill_id = $bill_id;
    $row_map_bill->subject = $bill_version_subject;
    
    // Does bill already exist?
    $map_bill_id = db_result(db_query("SELECT bill_id FROM {map_bill} WHERE bill_id = %d", $bill_id));
                                               
    if ($map_bill_id) {
      // already exists so update record
      drupal_write_record('map_bill', $row_map_bill, 'bill_id');
    } else {
      // does not exist so insert new record
      drupal_write_record('map_bill', $row_map_bill);
    } 

    watchdog('legislature', 'MAP bill record added: %bill_id.', array('%bill_id' => $bill_id), WATCHDOG_NOTICE);

  }

}

/**
 * Move California Legislature history, analysis, summary vote, and bill detail data into action tables. 
 * Affected table(s): legislature_action, legislature_action_politician
 */
function legislature_ca_move_actions($session = '2009', $clear = false, $suffix = '2009') {
  
  // Delete existing records from tables
  //if ($clear = TRUE) {
    //$session_obj = legislature_get_session('ca', $session);
    //db_query("DELETE la, lap
              //FROM legislature_bill AS lb
              //LEFT JOIN legislature_action AS la ON lb.bill_id = la.bill_id
              //LEFT JOIN legislature_action_politician AS lap ON la.action_id = lap.action_id
              //WHERE lb.session_id = %d", $session_obj->session_id));
  //}  
   
  // *** legislature_action table ***
  
  // Select all bill history entries in state table
  $result = db_query("SELECT * FROM {legislature_ca_bill_history}");
  
  // Cycle through bill history entries
  while ($row_legislature_ca_bill_history = db_fetch_object($result)) {
    
    // Get bill_id (mapped to state bill_id)
    $bill_id = db_result(db_query("SELECT leg_value FROM {legislature_ca_temp} 
                                     WHERE ca_name = 'bill_id' AND ca_value = '%s' AND leg_name = 'bill_id'", 
                                     $row_legislature_ca_bill_history->bill_id));
                                                         
    // Build location value
    $bill_action_location = $row_legislature_ca_bill_history->primary_location;
    $bill_action_location .= ' ' . $row_legislature_ca_bill_history->secondary_location;
    $bill_action_location .= ($row_legislature_ca_bill_history->ternary_location != '') ? ': ' : '';
    if ((string)$row_legislature_ca_bill_history->secondary_location == 'Committee') {
      $bill_action_location .= db_result(db_query("SELECT long_description FROM {legislature_ca_location_code}
                                  WHERE location_code = '%s'", $row_legislature_ca_bill_history->ternary_location));
    } else {
      $bill_action_location .= $row_legislature_ca_bill_history->ternary_location;
    }
    
    // Get history date, format as "20090123"                                        
    $history_date = gmstrftime("%Y%m%d", strtotime($row_legislature_ca_bill_history->action_date));

    // Does bill action already exist?
    $action_id = db_result(db_query("SELECT action_id FROM {legislature_action} 
                                     WHERE bill_id = %d AND date = '%s' AND motion = '%s'", 
                                     $bill_id, $history_date, $row_legislature_ca_bill_history->action));
                                         
    $row_legislature_action = new stdClass();
    $row_legislature_action->bill_id = $bill_id;
    $row_legislature_action->description = 'action';
    $row_legislature_action->date = $history_date;
    $row_legislature_action->motion = $row_legislature_ca_bill_history->action;
    $row_legislature_action->is_vote = 0;
    $row_legislature_action->location = $bill_action_location;
    $row_legislature_action->date_int = strtotime($row_legislature_ca_bill_history->action_date);
    $row_legislature_action->weight = $row_legislature_ca_bill_history->action_sequence;
    
    if ($action_id) {
      // already exists so update record
      $row_legislature_action->action_id = $action_id;
      drupal_write_record('legislature_action', $row_legislature_action, 'action_id');                                            
      watchdog('legislature', 'Action already exists: %action_id.', array('%action_id' => $action_id), WATCHDOG_NOTICE);
    } else {
      // does not exist so insert new record
      drupal_write_record('legislature_action', $row_legislature_action);
      $action_id = db_last_insert_id('{legislature_action}', 'action_id');
      watchdog('legislature', 'Action does not exist: %action_id.', array('%action_id' => $action_id), WATCHDOG_NOTICE);
    } 
                                                
    watchdog('legislature', 'Bill action (history) added: %action_id.', array('%action_id' => $action_id), WATCHDOG_NOTICE);
    
  }

  // Select all bill analysis entries in state table
  $result = db_query("SELECT * FROM {legislature_ca_bill_analysis}");
  
  // Cycle through bill analysis entries
  while ($row_legislature_ca_bill_analysis = db_fetch_object($result)) {
    
    // Get bill_id (mapped to state bill_id)
    $bill_id2 = db_result(db_query("SELECT leg_value FROM {legislature_ca_temp} 
                                     WHERE ca_name = 'bill_id' AND ca_value = '%s' AND leg_name = 'bill_id'", 
                                     $row_legislature_ca_bill_analysis->bill_id));
    
    // Get analysis document text
    $source_text = "";
    $antiword_avail = FALSE; // Set to TRUE if antiword conversion tool available (on Windows 64-bit, not avail)
    if ($antiword_avail) {
      $dir = variable_get('legislature_ca_data_directory', '') . '/pubinfo_'.$suffix;
      $file_name = $dir . '/' . $row_legislature_ca_bill_analysis->source_doc;
      // Convert file from Word format to plain text
      $antiword = variable_get('legislature_ca_antiword', '');
      $output = '-t';
      $command="$antiword $output $file_name";
      $source_text_lines = array();
      exec($command, $source_text_lines);
      $source_text = implode(PHP_EOL, $source_text_lines);
    }
                                         
    // Get analysis date, format as "20090123"                                        
    $analysis_date = gmstrftime("%Y%m%d", strtotime($row_legislature_ca_bill_analysis->analysis_date));
    
    // Does bill action already exist?
    $action_id2 = db_result(db_query("SELECT action_id FROM {legislature_action} 
                                     WHERE bill_id = %d AND date = '%s' AND description = 'analysis'", 
                                     $bill_id2, $analysis_date));
                                             
    $row_legislature_action = new stdClass();
    $row_legislature_action->bill_id = $bill_id2;
    $row_legislature_action->description = 'analysis';
    $row_legislature_action->date = $analysis_date;
    $row_legislature_action->date_int = strtotime($row_legislature_ca_bill_analysis->analysis_date);
    $row_legislature_action->document = $source_text;
    
    if ($action_id2) {
      // already exists so update record
      $row_legislature_action->action_id = $action_id2;
      drupal_write_record('legislature_action', $row_legislature_action, 'action_id');
      watchdog('legislature', 'Action already exists: %action_id.', array('%action_id' => $action_id2), WATCHDOG_NOTICE);
    } else {
      // does not exist so insert new record
      drupal_write_record('legislature_action', $row_legislature_action);
      $action_id2 = db_last_insert_id('{legislature_action}', 'action_id');
      watchdog('legislature', 'Action does not exist: %action_id.', array('%action_id' => $action_id2), WATCHDOG_NOTICE);
    } 
                                                
    watchdog('legislature', 'Bill action (analysis) added: %action_id.', array('%action_id' => $action_id2), WATCHDOG_NOTICE);
    
  }

  // Select all bill summary vote entries in state table
  $result = db_query("SELECT * FROM {legislature_ca_bill_summary_vote}");
  
  // Cycle through bill summary vote entries
  while ($row_legislature_ca_bill_summary_vote = db_fetch_object($result)) {
    
    // Get bill_id (mapped to state bill_id)
    $bill_id3 = db_result(db_query("SELECT leg_value FROM {legislature_ca_temp} 
                                     WHERE ca_name = 'bill_id' AND ca_value = '%s' AND leg_name = 'bill_id'", 
                                     $row_legislature_ca_bill_summary_vote->bill_id));
    
    // Get motion text
    $motion_text = db_result(db_query("SELECT motion_text FROM {legislature_ca_bill_motion} 
                                     WHERE motion_id = '%s'", 
                                     $row_legislature_ca_bill_summary_vote->motion_id));
                                     
    // Get summary vote date, format as "20090123"                                        
    $summary_vote_date = gmstrftime("%Y%m%d", strtotime($row_legislature_ca_bill_summary_vote->vote_date_time));
                                     
    // Get location code
    if ($row_legislature_ca_bill_summary_vote->location_code == 'AFLOOR') {
      $location = 'Assembly';
      $is_substantive = 1;
    } else if ($row_legislature_ca_bill_summary_vote->location_code == 'SFLOOR') {
      $location = 'Senate';
      $is_substantive = 1;
    } else {
      $location = db_result(db_query("SELECT description FROM {legislature_ca_location_code} 
                                     WHERE location_code = '%s'", 
                                     $row_legislature_ca_bill_summary_vote->location_code));
      $is_substantive = "";
    }
    
    // Does bill action already exist?
    $action_id3 = db_result(db_query("SELECT action_id FROM {legislature_action} 
                                     WHERE bill_id = %d AND date = '%s' AND location = '%s' AND description = 'vote'", 
                                     $bill_id3, $summary_vote_date, $location));
                                             
    $row_legislature_action = new stdClass();
    $row_legislature_action->bill_id = $bill_id3;
    $row_legislature_action->description = 'vote';
    $row_legislature_action->motion = $row_legislature_ca_bill_summary_vote->motion_text;
    $row_legislature_action->date = $summary_vote_date;
    $row_legislature_action->date_int = strtotime($row_legislature_ca_bill_summary_vote->vote_date_time);
    $row_legislature_action->location = $location;
    $row_legislature_action->ayes = $row_legislature_ca_bill_summary_vote->ayes;
    $row_legislature_action->noes = $row_legislature_ca_bill_summary_vote->noes;
    $row_legislature_action->outcome = preg_replace("/[()]/", "", $row_legislature_ca_bill_summary_vote->vote_result);
    $row_legislature_action->is_vote = 1;
    $row_legislature_action->is_substantive = $is_substantive;
    
    if ($action_id3) {
      // already exists so update record
      $row_legislature_action->action_id = $action_id3;
      drupal_write_record('legislature_action', $row_legislature_action, 'action_id');
      watchdog('legislature', 'Action already exists: %action_id.', array('%action_id' => $action_id3), WATCHDOG_NOTICE);
    } else {
      // does not exist so insert new record
      drupal_write_record('legislature_action', $row_legislature_action);
      $action_id3 = db_last_insert_id('{legislature_action}', 'action_id');
      watchdog('legislature', 'Action does not exist: %action_id.', array('%action_id' => $action_id3), WATCHDOG_NOTICE);
    } 
                                                
    // Map record to action_id using combination of fields
    $row_legislature_ca_temp = new stdClass();
    $row_legislature_ca_temp->ca_name = 'summary_vote_id';
    $summary_vote_id = $row_legislature_ca_bill_summary_vote->bill_id . 
                       $row_legislature_ca_bill_summary_vote->location_code .
                       $row_legislature_ca_bill_summary_vote->vote_date_seq .
                       $row_legislature_ca_bill_summary_vote->motion_id;
    $row_legislature_ca_temp->ca_value = $summary_vote_id;
    $row_legislature_ca_temp->leg_name = 'action_id';
    $row_legislature_ca_temp->leg_value = $action_id3;
    drupal_write_record('legislature_ca_temp', $row_legislature_ca_temp);
    
    watchdog('legislature', 'Bill action (summary vote) added: %action_id.', array('%action_id' => $action_id3), WATCHDOG_NOTICE);
    
  }
  
  // *** legislature_action_politician table ***
  
  // Select all bill detail vote entries in state table
  $result = db_query("SELECT * FROM {legislature_ca_bill_detail_vote}");
  
  // Cycle through bill detail vote entries
  while ($row_legislature_ca_bill_detail_vote = db_fetch_object($result)) {
    
    // Get action_id (mapped to summary_vote_id in temp table)
    // Can build id from same fields in legislature_ca_bill_detail_vote to map
    $summary_vote_id = $row_legislature_ca_bill_detail_vote->bill_id . 
                       $row_legislature_ca_bill_detail_vote->location_code .
                       $row_legislature_ca_bill_detail_vote->vote_date_seq .
                       $row_legislature_ca_bill_detail_vote->motion_id;
    $action_id4 = db_result(db_query("SELECT leg_value FROM {legislature_ca_temp} 
                                      WHERE ca_name = 'summary_vote_id' AND ca_value = '%s' AND leg_name = 'action_id'", 
                                      $summary_vote_id));
    
    // Get politician_id (mapped to legislator name)
    $politician_id = db_result(db_query("SELECT leg_value FROM {legislature_ca_temp} 
                                          WHERE ca_name = 'author_name' AND ca_value = '%s' AND leg_name = 'politician_id'", 
                                          $row_legislature_ca_bill_detail_vote->legislator_name));
                                          
    // Get vote
    $vote = ($row_legislature_ca_bill_detail_vote->vote_code == 'ABS') ? 'NV' : $row_legislature_ca_bill_detail_vote->vote_code;
                                                 
    $row_legislature_action_politician = new stdClass();
    $row_legislature_action_politician->politician_id = $politician_id;
    $row_legislature_action_politician->action_id = $action_id4;
    $row_legislature_action_politician->vote = $vote;
    
    drupal_write_record('legislature_action_politician', $row_legislature_action_politician);
                                                    
    watchdog('legislature', 'Bill action politician added: %politician_id.', array('%politician_id' => $politician_id), WATCHDOG_NOTICE);
    
  }
  
}

/**
 * Convert elements of an array with strings equal to "NULL" to empty strings ('').
 *
 * @param $array
 *   An array
 * @return
 *   The converted array.
 */
function nullstrings_to_empty($array) {
  $array_result = array();
  foreach($array as $element) {
    if($element == 'NULL') {
      $array_result[] = '';
    } else {
      $array_result[] = $element;
    }
  }
  return $array_result;
}

/**
 * Retrieves text from a file.
 *
 * @param $array
 *   A path and file name for a text file.
 * @return
 *   The text from the file.
 */
function text_from_file($file) {
  if (!is_readable($file)) {
    return NULL;
  }
  $fh = fopen($file, 'rb');
  $text = '';
  while (!feof($fh)) {
      $buffer = fgets($fh);
      $text .= $buffer;
  }
  return $text;
}
