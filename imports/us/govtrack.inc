<?php

function legislature_govtrack_politicians() {
  $genders = array(
    'M' => 'Male',
    'F' => 'Female',
  );
  $offices = array(
    'rep' => 'House',
    'sen' => 'Senate',
  );

  foreach (legislature_get_enabled_sessions('us') as $congress) {
    $response = drupal_http_request('http://www.govtrack.us/data/us/' . $congress . '/people.xml');
    if ($response->code !== '200') {
      watchdog('legislature', 'Failed to parse people.xml. @error', array('@error' => $result->error), WATCHDOG_ERROR);
      continue;
    }
    $xml = simplexml_load_string($response->data);
    foreach ($xml->person as $xperson) {
      $politician = array(
        'last_name' => (string) $xperson['lastname'],
        'full_name' => $xperson['firstname'] .' '. $xperson['lastname'],
        'middle_name' => (string) $xperson['middlename'],
        'nick_name' => (string) $xperson['nickname'],
        'first_name' => (string) $xperson['firstname'],
        'name_modifier' => (string) $xperson['namemod'],
        'roster_name' => (string) $xperson['name'],
        'gender' => $genders[(string) $xperson['gender']],
        'govtrack_id' => (string) $xperson['id'],
        'crp_id' => (string) $xperson['osid'],
        'bioguide_id' => (string) $xperson['bioguideid'],
      );
      $elected_jobs = array();
      foreach ($xperson->role as $xrole) {
        $type = (string) $xrole['type'];
        if ($type === 'rep' || $type === 'sen') {
          $elected_job = array(
            'district_id' => legislature_district('us', $offices[$type], (string) $xrole['state'], (string) $xrole['district'], ((string) $xrole['state']) . ' ' . $offices[$type] . ' district ' . ((string) $xrole['district'])),
            'office_id' => legislature_get_office_id('us', $offices[$type]),
            'is_candidate' => FALSE,
          );
          list($year, $month, $day) = explode('-', (string) $xrole['startdate']);
          $elected_job['start'] = gmmktime(0, 0, 0, $month, $day, $year, 0);
          list($year, $month, $day) = explode('-', (string) $xrole['enddate']);
          $elected_job['end'] = gmmktime(0, 0, 0, $month, $day, $year, 0);
          $elected_jobs[] = $elected_job;

          $politician['party'] = (string) $xrole['party'];
        }
      }
      $politician_id = legislature_politician($politician, 'govtrack');
      if (!is_null($politician_id)) {
        legislature_save_elected_jobs('us', $politician_id, $elected_jobs, $congress);
      }
    }
  }
}

function legislature_govtrack_bills($clear = array()) {
  foreach ($clear as $session => $clear_session) {
    if ($clear_session) {
      db_query("UPDATE {legislature_bill} b INNER JOIN {legislature_session} s ON s.session_id = b.session_id AND s.session_jurisdiction = 'us' AND s.session_name = '%s' SET last_version = ''", $session);
    }
  }

  foreach (legislature_get_enabled_sessions('us') as $congress) {
    legislature_govtrack_bills_start(NULL, 'setup', $congress);
    $result = drupal_http_request('http://www.govtrack.us/data/us/'. $congress .'/bills.index.xml');
    $parser = drupal_xml_parser_create($result->data);
    xml_set_element_handler($parser, 'legislature_govtrack_bills_start', 'legislature_govtrack_bills_end');
    xml_set_character_data_handler($parser, 'legislature_govtrack_bills_data');
    if (!xml_parse($parser, $result->data, 1)) {
      watchdog('legislature', t('Failed to parse bills.xml. @error', array('@error' => $result->error)));
    }
    xml_parser_free($parser);
  }
}

function legislature_govtrack_bills_start($parser, $name, $attributes) {
  static $session_id;

  switch ($name) {
    case 'setup':
      $session_id = db_result(db_query("SELECT session_id FROM {legislature_session} WHERE session_jurisdiction = 'us' AND session_name = '%s'", $attributes));
      break;

    case 'BILL':
      list($dummy, $title) = explode(': ', $attributes['TITLE'], 2);
      $bill = array(
        'session_id' => $session_id,
        'prefix' => drupal_strtoupper($attributes['TYPE']),
        'measure' => drupal_strtoupper($attributes['TYPE']) .' '. $attributes['NUMBER'],
        'topic' => $title,
        'description' => $attributes['OFFICIAL-TITLE'],
        'status' => $attributes['STATUS'],
      );
      if (isset($attributes['LAST-ACTION'])) {
        $bill['last_version'] = $attributes['LAST-ACTION'];
      }
      else {
        $bill['last_version'] = $attributes['LASTACTION'];
      }
      if (legislature_save_bill($bill)) {
        job_queue_add('legislature_govtrack_update_bill', 'Download bill %measure', array($bill['bill_id'], '%measure' => $bill['measure']), drupal_get_path('module', 'legislature') .'/imports/us/govtrack.inc', TRUE);
      }
      break;
  }
}

function legislature_govtrack_bills_end($parser, $name) {
}

function legislature_govtrack_bills_data($parser, $data) {
}

/**
 * Update a single bill.
 */
function legislature_govtrack_update_bill($bill_id) {
  $bill = db_fetch_object(db_query('SELECT b.bill_id, s.session_name, b.session_id, b.measure FROM {legislature_bill} b INNER JOIN {legislature_session} s ON s.session_id = b.session_id WHERE b.bill_id = %d', $bill_id));
  if ($bill === FALSE) {
    watchdog('legislature', t('Skipping bill updates for %bill_id because the bill is missing.', array('%bill_id' => $bill_id)));
    return;
  }

  // Retrieve bill XML.
  $url = 'http://www.govtrack.us/data/us/'. $bill->session_name .'/bills/'. implode('', explode(' ', drupal_strtolower($bill->measure))) .'.xml';
  $response = drupal_http_request($url);
  if ($response->code !== '200') {
    watchdog('legislature', t('Failed to update bill %id. (%url) %error', array('%id' => $bill_id, '%url' => $url, '%error' => $response->error)));
    job_queue_add('legislature_govtrack_update_bill', 'Download bill %id', array($bill->bill_id), drupal_get_path('module', 'legislature') .'/imports/us/govtrack.inc', TRUE);
    return;
  }
  $xml = simplexml_load_string($response->data);

  // Collect bill data.
  list($y, $m, $d) = explode('-', (string) $xml->introduced['datetime']);
  $bill->introduced = gmmktime(0, 0, 0, $m, $d, $y);
  drupal_write_record('legislature_bill', $bill, array('bill_id'));
  $actions = array();
  $substantive_rolls = array();
  $houses = array(
    's' => 'Senate',
    'h' => 'House',
  );
  foreach ($xml->actions->children() as $xml_tag => $xml_action) {
    preg_match('/(\d\d\d\d)-(\d\d)-(\d\d).*/', (string) $xml_action['datetime'], $matches);
    list(, $year, $month, $day) = $matches;
    $action = array(
      'bill_id' => $bill->bill_id,
      'description' => $xml_tag,
      'date' => preg_replace('/(\d\d\d\d)-(\d\d)-(\d\d).*/', '\1\2\3', (string) $xml_action['datetime']),
      'date_int' => isset($xml_action['date']) ? (int) $xml_action['date'] : gmmktime(0, 0, 0, $month, $day, $year),
      'ayes' => 0,
      'noes' => 0,
      'motion' => (string) $xml_action->text,
      'location' => '',
      'outcome' => '',
      'is_vote' => FALSE,
      'is_substantive' => FALSE,
      'vote_roll' => 0,
    );
    if ($action['description'] === 'vote') {
      if (isset($xml_action['roll'])) {
        // This will be imported with the roll votes.
        $substantive_rolls[(string) $xml_action['where']] = (int) $xml_action['roll'];
        continue;
      }
      else {
        $action['location'] = $houses[(string) $xml_action['where']];
        $action['outcome'] = drupal_strtoupper((string) $xml_action['result']);
        $action['is_vote'] = TRUE;
        $action['is_substantive'] = TRUE;
      }
    }
    $actions[] = $action;
  }

  $titles = array();
  foreach ($xml->titles->title as $xml_title) {
    $titles[] = array(
      'bill_id' => $bill->bill_id,
      'title' => (string) $xml_title,
      'type' => (string) $xml_title['type'],
      'status_as' => (string) $xml_title['as'],
    );
  }

  $sponsors = array();
  if (isset($xml->sponsor)) {
    $sponsors[] = array(
      'bill_id' => $bill->bill_id,
      'politician_id' => legislature_get_politician_id('govtrack', (int) $xml->sponsor['id']),
      'type' => LEGISLATURE_SPONSOR,
    );
  }
  foreach ($xml->cosponsors->cosponsor as $xml_cosponsor) {
    $sponsors[] = array(
      'bill_id' => $bill->bill_id,
      'politician_id' => legislature_get_politician_id('govtrack', (int) $xml_cosponsor['id']),
      'type' => LEGISLATURE_COSPONSOR,
    );
  }

  $committees = array();
  foreach ($xml->committees->committee as $xml_committee) {
    $committees[] = array(
      'bill_id' => $bill->bill_id,
      'committee_id' => db_result(db_query("SELECT committee_id FROM {legislature_committee_name} WHERE session_id = %d AND name = '%s'", $bill->session_id, (string) $xml_committee['name'])),
      'activity' => (string) $xml_committee['activity'],
    );
  }

  $related_bills = array();
  foreach ($xml->relatedbills->bill as $xml_bill) {
    $related_bills[] = array(
      'bill_id' => $bill->bill_id,
      'relation' => (string) $xml_bill['relation'],
      'session' => (string) $xml_bill['session'],
      'type' => (string) $xml_bill['type'],
      'number' => (int) $xml_bill['number'],
      'related_bill_id' => db_result(db_query("SELECT bill_id FROM {legislature_bill} lb INNER JOIN {legislature_session} ls ON lb.session_id = ls.session_id WHERE lb.measure = '%s' AND ls.session_name = '%s'", $xml_bill['type'] . ' ' . $xml_bill['number'], $xml_bill['session'])),
    );
  }

  $subjects = array();
  $first = TRUE;
  foreach ($xml->subjects->term as $xml_term) {
    $subjects[] = array(
      'bill_id' => $bill->bill_id,
      'first' => $first,
      'subject' => (string) $xml_term['name'],
    );
    $first = FALSE;
  }

  foreach ($xml->amendments->amendment as $xml_amendment) {
    legislature_govtrack_amendment($bill->session_name, $bill->bill_id, (string) $xml_amendment['number']);
  }

  // Retrieve roll votes XML.
  $url = 'http://www.govtrack.us/congress/votes_download_xml.xpd?bill='. implode($bill->session_name .'-', explode(' ', drupal_strtolower($bill->measure)));
  $response = drupal_http_request($url);
  if ($response->code !== '200') {
    watchdog('legislature', t('Failed to update vote for bill %id. (%url) %error', array('%id' => $bill_id, '%url' => $url, '%error' => $response->error)));
    job_queue_add('legislature_govtrack_update_bill', t('Download bill %id', array('%id' => $bill_id)), array($bill->bill_id), drupal_get_path('module', 'legislature') .'/imports/us/govtrack.inc', TRUE);
    return;
  }
  $xml = simplexml_load_string($response->data);

  // Collect roll votes data.
  foreach ($xml->votes->vote as $xml_vote) {
    $action = array(
      'bill_id' => $bill->bill_id,
      'description' => 'vote',
      'location' => $houses[(string) $xml_vote['chamber']],
      'is_vote' => TRUE,
      'is_substantive' => $substantive_rolls[(string) $xml_vote['chamber']] === (int) $xml_vote['roll'],
      'vote_roll' => (int) $xml_vote['roll'],
      'motion' => (string) $xml_vote->description,
      'date_int' => strtotime(((string) $xml_vote->date) . ' America/New_York')
    );
    $action['date'] = format_date($action['date_int'], 'custom', 'Ymd');
    $result = (string) $xml_vote->result;
    if (stristr($result, 'passed') !== FALSE || stristr($result, 'agreed') !== FALSE) {
      $action['outcome'] = 'PASS';
    }
    elseif (stristr($result, 'failed') !== FALSE || stristr($result, 'rejected') !== FALSE) {
      $action['outcome'] = 'FAIL';
    }
    else {
      watchdog('legislature', t('Vote outcome not found in %result', array('%result' => $result)), WATCHDOG_ERROR);
    }
    $actions[] = $action;
  }

  // Save bill data.
  db_query('DELETE FROM {legislature_bill_title} WHERE bill_id = %d', $bill->bill_id);
  foreach ($titles as $title) {
    drupal_write_record('legislature_bill_title', $title);
  }

  db_query('DELETE FROM {legislature_bill_sponsor} WHERE bill_id = %d', $bill->bill_id);
  foreach ($sponsors as $sponsor) {
    drupal_write_record('legislature_bill_sponsor', $sponsor);
  }

  db_query('DELETE FROM {legislature_bill_committee} WHERE bill_id = %d', $bill->bill_id);
  foreach ($committees as $committee) {
    drupal_write_record('legislature_bill_committee', $committee);
  }

  db_query('DELETE FROM {legislature_bill_relation} WHERE bill_id = %d', $bill->bill_id);
  foreach ($related_bills as $related_bill) {
    drupal_write_record('legislature_bill_relation', $related_bill);
  }
  // Fill in bill ID for bills related to this bill.
  list($type, $number) = explode(' ', $bill->measure);
  db_query("UPDATE {legislature_bill_relation} SET related_bill_id = %d WHERE session = %d AND type = '%s' AND number = %d AND related_bill_id = 0", $bill->bill_id, $bill->session_name, drupal_strtolower($type), $number);

  db_query('DELETE FROM {legislature_bill_subject} WHERE bill_id = %d', $bill->bill_id);
  foreach ($subjects as $subject) {
    drupal_write_record('legislature_bill_subject', $subject);
  }

  // Save actions data.
  $action_ids = array();
  foreach ($actions as $action) {
    legislature_save_action($action, array('bill_id', 'description', 'date', 'motion'));
    $action_ids[] = $action['action_id'];

    if ($action['is_vote']) {
      if ($action['vote_roll'] === 0) {
        db_query('DELETE FROM {legislature_action_politician} WHERE action_id = %d', $action['action_id']);

        // Vote actions imported here do not have full rolls, so we need to
        // simulate this and fill out the legislators' individual votes.
        db_query("INSERT INTO {legislature_action_politician} (politician_id, action_id, vote) SELECT politician_id, %d, '%s' FROM {legislature_elected_job} ej INNER JOIN {legislature_elected_job_session} ejs ON ej.elected_job_id = ejs.elected_job_id AND ejs.session_id = %d INNER JOIN {legislature_office} o ON o.office_id = ej.office_id AND o.title = '%s' INNER JOIN {legislature_district} d ON d.district_id = ej.district_id AND d.state NOT IN ('DC', 'AS', 'GU', 'VI', 'PR') WHERE ej.start < %d AND %d < ej.end", $action['action_id'], ($action['outcome'] == 'PASS') ? 'AYE' : 'NOE', $bill->session_id, $action['location'], $action['date_int'], $action['date_int']);
        $action[$action['outcome'] === 'PASS' ? 'ayes' : 'noes'] = db_affected_rows();
        legislature_save_action($action);
      }
      else {
        legislature_govtrack_roll($action['action_id']);
      }
    }
  }
  // Delete actions which we have not just imported.
  db_query("DELETE FROM {legislature_action} WHERE bill_id = %d AND vote_roll != 0 AND amendment_id = 0 AND action_id NOT IN (". db_placeholders($action_ids) .")", array_merge(array($bill->bill_id), $action_ids));

  module_invoke_all('legislature', 'bill', 'update ran', array('bill_id' => $bill_id));
}

function legislature_govtrack_amendment($session, $bill_id, $amendment_number) {
  // Get amendment XML
  $url = 'http://www.govtrack.us/data/us/'. $session .'/bills.amdt/'. $amendment_number .'.xml';
  $response = drupal_http_request($url);
  if ($response->code !== '200') {
    watchdog('legislature', 'Failed to parse XML for Amendment ' . $amendment_number . '. @error', array('@error' => $result->error), WATCHDOG_ERROR);
    return;
  }

  // Parse XML to get data
  $xml = simplexml_load_string($response->data);
  $sponsor_id = (isset($xml->sponsor['id'])) ? legislature_get_politician_id('govtrack', (int) $xml->sponsor['id']) : '';
  $amendment = array(
    'bill_id' => $bill_id,
    'sponsor_politician_id' => $sponsor_id,
    'location' => $xml['chamber'],
    'number' => $xml['number'],
    'date' => $xml->offered['date'],
    'sequence' => $xml->amends['sequence'],
    'description' => $xml->description,
    'purpose' => $xml->purpose,
    'status' => $xml->status,
    'status_date' => $xml->status['date'] ,
  );

  // Does amendment record already exist?
  $amendment_id = db_result(db_query("SELECT a.amendment_id FROM {legislature_amendment} a
                                      INNER JOIN {legislature_bill} b ON b.bill_id = a.bill_id
                                      INNER JOIN {legislature_session} s ON s.session_id = b.session_id
                                      WHERE a.bill_id = %d AND a.location = %d AND a.number = %d AND s.session_name = %d",
                                      $amendment['bill_id'], $amendment['location'], $amendment['number'], $session));

  if ($amendment_id) {
    // already exists so update record
    $amendment['amendment_id'] = $amendment_id;
    if (drupal_write_record('legislature_amendment', $amendment, 'amendment_id')) {
      watchdog('legislature', 'Updated amendment record ID @id', array('@id' => $amendment_id), WATCHDOG_NOTICE);
    }
  }
  else {
    // does not exist so insert new record
    if (drupal_write_record('legislature_amendment', $amendment)) {
      $amendment_id = db_last_insert_id('legislature_amendment', 'amendment_id');
      watchdog('legislature', 'Inserted new amendment record ID @id', array('@id' => $amendment_id), WATCHDOG_NOTICE);
    }
  }

  // Array to hold amendment actions
  $amendment_actions = array();

  // Cycle through amendment actions, which can be 'action', 'vote' or 'withdraw'
  foreach ($xml->actions->children() as $xaction) {
    switch ($xaction->getName()) {
      case "action":
        $action = array(
          'amendment_id' => $amendment_id,
          'bill_id' => $bill_id,
          'description' => 'action',
          'date' => date('Ymd', (int) $xaction['date']),
          'date_int' => $xaction['date'],
          'motion' => $xaction->text,
        );
        break;

      case "vote":
        $action = array(
          'amendment_id' => $amendment_id,
          'bill_id' => $bill_id,
          'description' => 'vote',
          'date' => date('Ymd', (int) $xaction['date']),
          'date_int' => $xaction['date'],
          'motion' => $xaction->text,
          'outcome' => drupal_strtoupper($xaction['result']),
          'is_vote' => 1,
        ); // Need to also deal with other vote details, how/roll
        break;

      case "withdrawn":
        $action = array(
          'amendment_id' => $amendment_id,
          'bill_id' => $bill_id,
          'description' => 'withdrawn',
          'date' => date('Ymd', (int) $xaction['date']),
          'date_int' => $xaction['date'],
          'motion' => $xaction->text,
        );
        break;
    }
    $amendment_actions[] = $action;
  }

  // Insert $amendment_actions into legislature_amendment_action
  foreach ($amendment_actions as $action) {
    // Does action record already exist?
    $action_id = db_result(db_query("SELECT a.action_id FROM {legislature_action} a
                                    INNER JOIN {legislature_bill} b ON b.bill_id = a.bill_id
                                    INNER JOIN {legislature_session} s ON s.session_id = b.session_id
                                    WHERE a.bill_id = %d AND a.amendment_id = %d AND a.description = '%s' AND a.date = %d AND s.session_name = %d",
                                    $action['bill_id'], $action['amendment_id'], $action['description'], $action['date'], $session));

    if ($action_id) {
      // already exists so update record
      $action['action_id'] = $action_id;
      if (drupal_write_record('legislature_action', $action, 'action_id')) {
        watchdog('legislature', 'Updated action record ID @id', array('@id' => $action_id), WATCHDOG_NOTICE);
      }
    }
    else {
      // does not exist so insert new record
      if (drupal_write_record('legislature_action', $action)) {
        $action_id = db_last_insert_id('legislature_action', 'action_id');
        watchdog('legislature', 'Inserted new action record ID @id', array('@id' => $action_id), WATCHDOG_NOTICE);
      }
    }
  }

  // Insert full text of amenedment into database
  $url = 'http://www.govtrack.us/data/us/'. $session .'/bills.amdt/'. $amendment_number .'.txt';
  $response = drupal_http_request($url);
  if ($response->code === '200') {
    $amendment = new StdClass();
    $amendment->amendment_id = db_result(db_query("SELECT amendment_id FROM {legislature_amendment} WHERE location = '%s' AND number = %d AND bill_id = %d", $amendment_number[0], substr($amendment_number, 1), $bill_id));
    $amendment->full_text = $response->data;
    drupal_write_record('legislature_amendment', $amendment, array('amendment_id'));
    watchdog('legislature', 'Inserted full text for amendment record ID @id', array('@id' => $amendment->amendment_id), WATCHDOG_NOTICE);
  }
}

function legislature_govtrack_amendment_xml($op, $parser, $data, $attributes = NULL) {
  static $amendment;
  static $last_start;

  switch ($op) {
    case 'setup':
      $amendment = array(
        'bill_id' => $data,
      );

    case 'start':
      switch ($data) {
        case 'AMENDMENT':
          $amendment['location'] = $attributes['CHAMBER'];
          $amendment['number'] = $attributes['NUMBER'];
          break;

        case 'AMENDS':
          $amendment['sequence'] = $attributes['SEQUENCE'];
          break;

        case 'SPONSOR':
          $amendment['sponsor_politician_id'] = legislature_get_politician_id('govtrack', $attributes['ID']);
          break;

        case 'OFFERED':
          $amendment['date'] = $attributes['DATE'];
          break;

        case 'DESCRIPTION':
        case 'PURPOSE':
          $last_start = $data;
          break;
      }
      break;

    case 'data':
      switch ($last_start) {
        case 'DESCRIPTION':
          $amendment['description'] = $data;
          break;

        case 'PURPOSE':
          $amendment['purpose'] = $data;
          break;
      }
      $last_start = NULL;
      break;

    case 'end':
      switch ($data) {
        case 'AMENDMENT':
          legislature_save_amendment($amendment);
          break;
      }
      break;
  }
}

function legislature_govtrack_bill_texts() {
  set_time_limit(0);
  $result = db_query("SELECT session_name FROM {legislature_session} WHERE session_jurisdiction = 'us'");
  while ($session = db_fetch_object($result)) {
    $session_directory = opendir(variable_get('legislature_govtrack_bill_text_directory', '') .'/'. $session->session_name);
    while (($directory = readdir($session_directory)) !== FALSE) {
      if ($directory != '.' && $directory != '..') {
        $texts_directory = opendir(variable_get('legislature_govtrack_bill_text_directory', '') .'/'. $session->session_name .'/'. $directory);
        $files = array();
        while (($file = readdir($texts_directory)) !== FALSE) {
          if ($file != '.' && $file != '..') {
            $files[] = $file;
          }
        }
        foreach (preg_grep('/^'. $directory .'\d*.txt$/', $files) as $bill) {
          $bill = str_replace('.txt', '', $bill);
          job_queue_add('legislature_govtrack_bill_text', t('Import bill text for %measure', array('%measure' => $bill)), array($session->session_name, $directory, $bill, preg_grep('/^'. $bill .'[a-z]*\.gen\.html$/', $files)), drupal_get_path('module', 'legislature') .'/imports/us/govtrack.inc', TRUE);
        }
        closedir($texts_directory);
      }
    }
    closedir($session_directory);
  }
}

function legislature_govtrack_roll($action_id) {
  $action = db_fetch_object(db_query('SELECT a.action_id, s.session_name, a.location, a.date_int, a.vote_roll, a.bill_id FROM {legislature_action} a INNER JOIN {legislature_bill} b ON b.bill_id = a.bill_id INNER JOIN {legislature_session} s ON s.session_id = b.session_id WHERE a.action_id = %d', $action_id));
  if (empty($action)) {
    watchdog('legislature', t('Failed to update roll vote for action %id, action does not exist.', array('%id' => $action_id)));
    return;
  }
  $response = drupal_http_request('http://www.govtrack.us/data/us/'. $action->session_name .'/rolls/'. drupal_strtolower(substr($action->location, 0, 1)) . format_date($action->date_int, 'custom', 'Y') .'-'. $action->vote_roll .'.xml');
  if ($response->code !== '200') {
    watchdog('legislature', t('Failed to update roll vote for action %id.', array('%id' => $action_id)));
    job_queue_add('legislature_govtrack_roll', t('Update roll vote for action %id (requeued).', array('%id' => $action_id)), array($action_id), drupal_get_path('module', 'legislature') .'/imports/us/govtrack.inc', TRUE);
    return;
  }

  $votes = array(
    '+' => 'AYE',
    '-' => 'NOE',
    '0' => 'NV',
    'P' => 'NV',
  );
  $xml = simplexml_load_string($response->data);
  $action->ayes = (int) $xml['aye'];
  $action->noes = (int) $xml['nay'];
  $action->required = (string) $xml->required;
  if (isset($xml->amendment)) {
    if (((string) $xml->amendment['ref']) === 'regular') {
      $number = (string) $xml->amendment['number'];
      $action->amendment_id = db_result(db_query("SELECT amendment_id FROM {legislature_amendment} WHERE location = '%s' AND number = %d AND bill_id = %d", $number[0], substr($number, 1), $action->bill_id));
    }
    else {
      $action->amendment_id = db_result(db_query("SELECT amendment_id FROM {legislature_amendment} WHERE sequence = %d AND bill_id = %d", (int) $xml->amendment['number'], $action->bill_id));
    }
  }
  drupal_write_record('legislature_action', $action, array('action_id'));
  db_query('DELETE FROM {legislature_action_politician} WHERE action_id = %d', $action->action_id);
  foreach ($xml->voter as $xvoter) {
    $politician_id = legislature_get_politician_id('govtrack', (int) $xvoter['id']);
    db_query("INSERT INTO {legislature_action_politician} (politician_id, action_id, vote) VALUES (%d, %d, '%s')", $politician_id, $action->action_id, $votes[(string) $xvoter['vote']]);
  }
}

function legislature_govtrack_committees() {
  include_once drupal_get_path('module', 'legislature') .'/xml.inc';

  foreach (legislature_get_enabled_sessions('us') as $session) {
    $response = drupal_http_request('http://www.govtrack.us/data/us/'. $session .'/committees.xml');
    if (!legislature_xml_parse($response->data, 'legislature_govtrack_committees_xml', array('session' => $session))) {
      watchdog('legislature', t('Failed to update committees for %id.', array('%id' => $session)));
    }
  }
  watchdog('legislature', t('Imported committees from Govtrack'));
}

function legislature_govtrack_committees_xml($op, $parser, $data, $attributes = NULL) {
  static $session_id, $committee_id, $committee_name_session;
  static $sessions = array();

  switch ($op) {
    case 'setup':
      $result = db_query("SELECT session_id, session_name FROM {legislature_session} WHERE session_jurisdiction = 'us'");
      while ($session = db_fetch_object($result)) {
        $sessions[$session->session_name] = $session->session_id;
      }

      $session_id = $sessions[$data['session']];
      break;

    case 'start':
      switch ($data) {
        case 'COMMITTEE':
          if ($attributes['TYPE'] != 'joint') {
            $office_id = legislature_get_office_id('US', drupal_ucfirst($attributes['TYPE']));
            $committee_id = legislature_committee($office_id, $session_id, $attributes['DISPLAYNAME'], $attributes['URL']);
            db_query('DELETE FROM {legislature_committee_membership} WHERE committee_id = %d', $committee_id);
            db_query('DELETE FROM {legislature_committee_name} WHERE committee_id = %d', $committee_id);

            // Found only in 109th.
            if (isset($attributes['THOMASNAME'])) {
              db_query("INSERT INTO {legislature_committee_name} (committee_id, session_id, name) VALUES (%d, %d, '%s')", $committee_id, $session_id, drupal_ucfirst($attributes['TYPE']) .' '. $attributes['THOMASNAME']);
            }
          }
          else {
            $committee_id = NULL;
          }
          break;

        case 'SUBCOMMITTEE':
          $committee_id = NULL;
          break;

        case 'MEMBER':
          if (!is_null($committee_id)) {
            $politician_id = legislature_get_politician_id('govtrack', $attributes['ID']);
            if ($politician_id == 0) {
              watchdog('legislature', t('Could not find politician %politician_id.', array('%politician_id' => $attributes['ID'])), WATCHDOG_WARNING);
            }
            else {
              db_query("INSERT INTO {legislature_committee_membership} (politician_id, committee_id, position) VALUES (%d, %d, '%s')", $politician_id, $committee_id, $attributes['ROLE']);
            }
          }
          break;

        case 'NAME':
          // Under THOMAS-NAMES in 110th only.
          if (!is_null($committee_id)) {
            $committee_name_session = $sessions[$attributes['SESSION']];
          }
          break;
      }
      break;

    case 'data':
      if ($committee_name_session == $session_id) {
        db_query("INSERT INTO {legislature_committee_name} (committee_id, session_id, name) VALUES (%d, %d, '%s')", $committee_id, $committee_name_session, $data);
        $committee_name_session = NULL;
      }
      break;
  }
}

/**
 * Look for new votes in the current session and schedule bill updates.
 */
function legislature_govtrack_recent_votes() {
  include_once drupal_get_path('module', 'legislature') .'/xml.inc';

  $response = drupal_http_request('http://www.govtrack.us/congress/votes_download_xml.xpd');
  if (!legislature_xml_parse($response->data, 'legislature_govtrack_recent_votes_xml')) {
    watchdog('legislature', t('Failed to download recent votes from Govtrack'));
  }
}

function legislature_govtrack_recent_votes_xml($op, $parser, $data, $attributes = NULL) {
  static $houses = array(
    's' => 'Senate',
    'h' => 'House',
  );
  static $done = FALSE;
  static $session = NULL;

  if (!$done) {
    switch ($op) {
      case 'start':
        switch ($data) {
          case 'VOTE':
            $action_id = db_result(db_query("SELECT action_id FROM {legislature_action} WHERE date_int > %d AND location = '%s' AND vote_roll = %d", gmmktime(0, 0, 0, 1, 1, $attributes['YEAR']), $houses[$attributes['CHAMBER']], $attributes['ROLL']));
            if ($action_id === FALSE) {
              if (is_null($session)) {
                $session = max(legislature_get_enabled_sessions('us'));
              }
              // We do not have this vote yet, need to find the bill to update.
              // Unfortunately, that is not stored in a machine-readable format
              // in this file. Download the vote's XML file.
              $response = drupal_http_request('http://www.govtrack.us/data/us/'. $session .'/rolls/'. $attributes['CHAMBER'] . $attributes['YEAR'] .'-'. $attributes['ROLL'] .'.xml');
              if (!legislature_xml_parse($response->data, 'legislature_govtrack_recent_votes_bill_xml', array('session' => $session))) {
                watchdog('legislature', t('Failed to download a recent roll from Govtrack'));
              }
            }
            else {
              // Since votes stored in chonological order, we can assume we have
              // all of the remaining votes.
              $done = TRUE;
            }
            break;
        }
        break;
    }
  }
}

function legislature_govtrack_recent_votes_bill_xml($op, $parser, $data, $attributes = NULL) {
  static $session;
  static $bill_ids = array();

  switch ($op) {
    case 'setup':
      $session = $data['session'];
      break;

    case 'start':
      switch ($data) {
        case 'BILL':
          $bill = db_fetch_object(db_query("SELECT b.bill_id FROM {legislature_bill} b INNER JOIN {legislature_session} s ON s.session_id = b.session_id WHERE s.session_name = '%s' AND s.session_jurisdiction = 'us' AND measure = '%s'", $session, drupal_strtoupper($attributes['TYPE'] .' '. $attributes['NUMBER'])));
          if ($bill !== FALSE) {
            if (!in_array($bill->bill_id, $bill_ids)) {
              job_queue_add('legislature_govtrack_update_bill', t('Download bill %id', array('%id' => $bill->bill_id)), array($bill->bill_id), drupal_get_path('module', 'legislature') .'/imports/us/govtrack.inc', TRUE);
              // Avoid adding the same bill twice.
              $bill_ids[] = $bill->bill_id;
            }
          }
          break;
      }
      break;
  }
}

/**
 * Find undervotes, senate and house with less than 100 and 435 respectively.
 */
function legislature_govtrack_undervotes() {
  $bill_ids = array();

  foreach (array('House' => 100, 'Senate' => 435) as $location => $count) {
    $result = db_query("SELECT a.bill_id, a.location, count(*) c FROM {legislature_action} a INNER JOIN {legislature_bill} b ON b.bill_id = a.bill_id INNER JOIN {legislature_session} s ON s.session_id = b.session_id AND s.session_jurisdiction = 'us' LEFT JOIN {legislature_action_politician} ap ON ap.action_id = a.action_id WHERE a.location = '%s' GROUP BY a.action_id HAVING c != %d", $location, $count);
    while ($action = db_fetch_object($result)) {
      $bill_ids[$action->bill_id] = TRUE;
    }
  }

  foreach ($bill_ids as $bill_id => $dummy) {
    job_queue_add('legislature_govtrack_update_bill', t('Download bill %id', array('%id' => $bill_id)), array($bill_id), drupal_get_path('module', 'legislature') .'/imports/us/govtrack.inc', TRUE);
  }
}
