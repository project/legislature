<?php

/**
 * @file Parse committees from Thomas, House, and Senate websites.
 *
 * Based on work by GovTrack.us. http://www.govtrack.us/source.xpd
 */

function legislature_us_thomas_committees() {
  $committees = array();

  // Get Thomas names
  foreach (legislature_get_enabled_sessions('us') as $session) {
    $response = drupal_http_request('http://thomas.loc.gov/bss/d'. $session .'query.html');
    preg_match_all('/<option value="\s*([^">]*)\{([hs])([a-z]+)([0-9]+)\}">-*.*<\/option>/', $response->data, $matches);
    if (empty($matches)) {
      watchdog('legislature', t('FEC committees: Failed to parse Thomas names.'));
      return;
    }
    foreach (array_keys($matches[0]) as $key) {
      $name = preg_replace('/  +/', ' ', $matches[1][$key]);
      $chamber = $matches[2][$key];
      $id = $matches[3][$key];
      $subcommittee_id = $matches[4][$key];

      if ($subcommittee_id == 0) {
        $committees[$chamber . $id][$session]['thomas_name'] = $name;
        $committees[$chamber . $id][$session]['members'] = array();
      }
      else {
        $committees[$chamber . $id][$session]['subcommittees'][$subcommittee_id]['thomas_name'] = $name;
      }
    }
  }

  $session_name = max(legislature_get_enabled_sessions('us'));
  $session = legislature_get_session('us', $session_name);

  // Get Senate committees
  $office_id = legislature_get_office_id('US', 'Senate');
  $response = drupal_http_request('http://www.senate.gov/pagelayout/committees/b_three_sections_with_teasers/membership.htm');
  preg_match_all('/="\/general\/committee_membership\/committee_memberships_(\w+?)\.htm">(.*?)</', $response->data, $matches);
  if (empty($matches)) {
    watchdog('legislature', t('FEC committees: Failed to parse Senate committees.'));
    return;
  }
  foreach (array_keys($matches[0]) as $key) {
    $code = drupal_strtolower($matches[1][$key]);
    $display_name = $matches[2][$key];
    $type = 'Senate';
    if (preg_match('/^Joint/', $display_name)) {
      $type = 'Joint';
    }
    elseif (strpos($display_name, 'Senate') === FALSE) {
      $display_name = 'Senate '. $display_name;
    }
    if (strpos($display_name, 'Commission') !== FALSE) {
      continue;
    }
    $committees[$code]['covered'] = TRUE;
    $committees[$code]['display name'] = $display_name;
    $response = drupal_http_request('http://www.senate.gov/general/committee_membership/committee_memberships_'. drupal_strtoupper($code) .'.htm');
    if (isset($response->error) || $response->code != 200) {
      watchdog('legislature', t('FEC committees: Could not get Senate committee info for %code.', array('%code' => $code)));
      return;
    }
    $html = preg_replace('/<\/position>\r?\n/', '</position>', $response->data);
    $html = preg_replace('/<\/state>\)\s+,/', '</state>),', $html);
    $committees[$code]['url'] = '';
    if (preg_match('/<span class="contenttext"><a href="(http:\/\/.*?senate.gov\/.*?)">/', $html, $matches2)) {
      $committees[$code]['url'] = $matches2[1];
    }

    $subcommittee_code = $code;
    $subcommittee_id = NULL;
    $subcommittee_name = NULL;
    $subcommittee_count = 0;
    $after_main = FALSE;
    foreach (preg_split('/\r?\n/', $html) as $line) {
      if (preg_match('/<A NAME="([^"]+)">/i', $line, $matches2)) {
        $subcommittee_code = $matches2[1];
        if (!preg_match('/^'. $code .'(\d+)$/', drupal_strtolower($subcommittee_code), $matches2)) {
          watchdog('legislature', t('FEC committees: Invalid subcommittee code %code.', array('%code' => $subcommittee_code)));
          return;
        }
        $subcommittee_id = (int) $matches2[1];
        $subcommittee_code = $code . $subcommittee_id;
      }
      if ($after_main && preg_match('/<subcommittee_name>(.*)<\/subcommittee_name>/', $line, $matches2)) {
        $subcommittee_count += 1;
        $subcommittee_name = preg_replace('/^(Permanent )?Subcommittee on /', '', $matches2[1]);
        $subcommittee_name = preg_replace('/  /', ' ', $subcommittee_name);
        $subcommittee_thomasname = $committees[$code][$session->name]['subcommittees'][$subcommittee_id]['thomas_name'];
        $committees[$code][$session->name]['subcommittees'][$subcommittee_id]['display name'] = $subcommittee_name;
        if ($subcommittee_thomasname == '') {
          $subcommittee_thomasname = $subname;
        }
        continue;
      }
      if (preg_match('/<pub_last>(.*)<\/pub_last>, <pub_first>(.*)<\/pub_first> \(<state>(.*)<\/state>\)(, <position>(.*)<\/position>)?/', $line, $matches2)) {
        $last_name = $matches2[1];
        $first_name = $matches2[2];
        $state = $matches2[3];
        $position = preg_replace('/ +$/', '', $matches2[5]);
        if ($position == 'Ranking') {
          $position = 'Ranking Member';
        }
        $politician_id = legislature_search_politician_id(array(
          'office id' => $office_id,
          'district id' => legislature_get_district_id('us', $state, 'Senate', ''),
          'name' => $last_name .', '. $first_name,
          'session id' => $session->session_id,
        ));
        if (is_null($politician_id)) {
          watchdog('legislature', t('FEC committees: %state Senator %name not found.', array('%state' => $state, '%name' => $last_name .', '. $first_name)));
        }
        else {
          if ($subcommittee_code == $code) {
            $committees[$code][$session->name]['members'][$politician_id] = $position;
          }
        }
        $after_main = TRUE;
      }
      elseif (preg_match('/^\s+(<\/td>)?<td nowrap="nowrap">/', $line)) { // The minority members are crammed onto one line.
        $line = preg_replace('/^\s+(<\/td>)?<td nowrap="nowrap">/', '', $line);
        foreach (explode('<br>', $line) as $x) {
          if (preg_match('/(.*?) \((\w\w)\)(, <position>(.*?)<\/position>)?/', $x, $matches2)) {
            $politician_name = $matches2[1]; // Last, First
            $state = $matches2[2];
            $position = $matches2[4];
            if ($position == 'Ranking') {
              $position = 'Ranking Member';
            }
            $politician_id = legislature_search_politician_id(array(
              'office id' => $office_id,
              'district id' => legislature_get_district_id('us', $state, 'Senate', ''),
              'name' => $politician_name,
              'session id' => $session->session_id,
            ));
            if (is_null($politician_id)) {
              watchdog('legislature', t('FEC committees: %state Senator %name not found.', array('%state' => $state, '%name' => $politician_name)));
            }
            else {
              if ($subcommittee_code == $code) {
                $committees[$code][$session->name]['members'][$politician_id] = $position;
              }
            }
          }
        }
        $after_main = TRUE;
      }
    }
  }

  // Get House committees
  $office_id = legislature_get_office_id('US', 'House');
  $response = drupal_http_request('http://clerk.house.gov/committee_info/index.html');
  if (isset($response->error)) {
    watchdog('legislature', t('FEC committees: Could not get House committee list.'));
    return;
  }
  preg_match_all('/\/committee_info\/index\.html\?comcode=([A-Z0]{3})00">(.*?)<\//', $response->data, $matches);
  foreach (array_keys($matches[0]) as $key) {
    $house_code = $matches[1][$key];
    $display_name = $matches[2][$key];
    $type = 'House';
    if (preg_match('/^Joint/', $display_name)) {
      $type = 'Joint';
    }
    elseif (strpos($display_name, 'House') === FALSE) {
      $display_name = 'House '. $display_name;
    }
    $our_code = drupal_strtolower(preg_replace('/^H/', 'HS', $house_code));
    $translate_code = array(
      'JEC' => 'jsec',
      'HJL' => 'jslc',
      'HJP' => 'jspr',
      'HIT' => 'jstx',
      'HM0' => 'hshm',
      'HIG' => 'hlig',
    );
    if (isset($translate_code[$house_code])) {
      $our_code = $translate_code[$house_code];
    }
    if (!isset($committees[$our_code]['covered'])) {
      $committees[$our_code]['covered'] = TRUE;
      $committees[$our_code]['display name'] = $display_name;
    }
    else {
      // this is joint committee already imported by the senate
    }
    $response = drupal_http_request('http://clerk.house.gov/committee_info/index.html?comcode=' . $house_code . '00');
    if (isset($response->error)) {
      watchdog('legislature', t('FEC committees: Could not get House committee info for %code', array('%code' => $our_code)));
    }
    $html = preg_replace('/(mem_contact_info[^>]+>)\s+/', '$1', $response->data);
    $html = preg_replace('/<\/li><li>/', "</li>\n<li>", $html);
    $subcommittee_names = array();
    preg_match_all('/href="index.html\?subcomcode='. $house_code .'(\d\d)">(.*?)<\//', $html, $matches2);
    foreach (array_keys($matches2[0]) as $key) {
      $subcommittee_names[$matches2[1][$key]] = preg_replace('/ Subcommittee$/', '', $matches2[2][$key]);
    }
    foreach (array_merge(array(NULL), array_keys($subcommittee_names)) as $subcommittee_id) {
      $code = NULL;
      $subcommittee_name = NULL;
      if (is_null($subcommittee_id)) {
        // Main committee
        $code = $our_code;
      }
      else {
        $response = drupal_http_request('http://clerk.house.gov/committee_info/index.html?subcomcode='. $house_code . $subcommittee_id);
        if (isset($response->error)) {
          watchdog('legislature', t('FEC committees: could not get House subcommittee info for %code.', array('%code' => $our_code . $subcommittee_id)));
        }
        $html = $response->data;
        $code = $our_code . $subcommittee_id;
        $subcommittee_name = $subcommittee_names[$subcommittee_id];
        $subcommittee_thomasname = $committees[$our_code][$session->name]['subcommittees'][$subcommittee_id]['thomas_name'];
        $committees[$our_code][$session->name]['subcommittees'][$subcommittee_id]['display name'] = $subcommittee_name;
        if ($subcommittee_thomasname == '') {
          $subcommittee_thomasname = $subcommittee_name;
        }
      }
      $state = 0;
      $rank = -1;
      foreach (preg_split('/[\n\r]+/', $html) as $line) {
        $line = preg_replace('/<\/?em>/', '', $line);
        if (strpos($line, '<div id="secondary_group">') !== FALSE) {
          $state = 2;
          $rank = 1;
        }
        if (strpos($line, '<div id="primary_group">') !== FALSE) {
          $state = 1;
          $rank = 1;
        }
        if (preg_match('/mem_contact_info\.html\?statdis=([^"]+)">(.*?)\s*<\/a>(, \w\w, ([\w\s]+))?/', $line, $matches2)) {
          $state_district = $matches2[1];
          $politician_name = $matches2[2];
          $position = $matches2[4]; // First Last
          $politician_name = drupal_convert_to_utf8($politician_name, 'ISO-8859-1');
          $politician_name = preg_replace('/  +/', ' ', $politician_name);
          if ($rank++ == 1 && $type != 'Joint') {
            if ($state == 1) {
              $position = 'Chair';
            }
            elseif ($state == 2) {
              $position = 'Ranking Member';
            }
            else {
              watchdog('legislature', t('FEC committees: bad state.'));
              return;
            }
          }
          if (!preg_match('/([A-Z]{2})([0-9]{2})/', $state_district, $matches2)) {
            watchdog('legislature', t('FEC committees: bad state/district %state_district.', array('%state_district' => $state_district)));
            return;
          }
          $state = $matches2[1];
          $district = (int) $matches2[2];
          $politician_id = legislature_search_politician_id(array(
            'office id' => $office_id,
            'district id' => legislature_get_district_id('us', $state, 'House', $district),
            'first last name' => $politician_name,
            'session id' => $session->session_id,
          ));
          if (is_null($politician_id)) {
            watchdog('legislature', t('FEC committees: %state-%district Representative %name not found.', array('%state' => $state, '%district' => $district, '%name' => $politician_name)));
          }
          else {
            if (is_null($subcommittee_id)) {
              $committees[$code][$session->name]['members'][$politician_id] = $position;
            }
          }
        }
      }
    }
  }

  // Add missing records
  // Then add records that we saw on Thomas but not on the Senate/House sites
  // (cause House took ages to update at the start of 2007). These committees
  // are obsolete.
  foreach (array_keys($committees) as $code) {
    if (!isset($committees[$code]['covered'])) {
      if ($code{0} == 'h') {
        $type = 'House';
      }
      else {
        $type = 'Senate';
      }
      $name = '';
      foreach (legislature_get_enabled_sessions('us') as $session) {
        if (!empty($committees[$code][$session]['thomas_name'])) {
          $name = $committees[$code][$session]['thomas_name'];
        }
      }
      $committees[$code]['display name'] = $type .' Committee on '. $name;
    }
  }

  // Save the committees
  $offices = array(
    'h' => 'House',
    's' => 'Senate',
  );
  foreach ($committees as $code => $committee) {
    if (isset($offices[$code{0}])) {
      $office_id = legislature_get_office_id('US', $offices[$code{0}]);
      foreach (legislature_get_enabled_sessions('us') as $session_name) {
        if (isset($committee[$session_name])) {
          $session = legislature_get_session('us', $session_name);
          $committee_id = legislature_committee($office_id, $session->session_id, $committee['display name'], $committee['url']);

          // Save Thomas name
          if (isset($committee[$session_name]['thomas_name'])) {
            $thomas_name = $committee[$session_name]['thomas_name'];
            if (strpos($thomas_name, $offices[$code{0}]) !== 0) {
              $thomas_name = $offices[$code{0}] .' '. $thomas_name;
            }
          }
          else {
            $thomas_name = $committee['display_name'];
          }
          legislature_committee_name($committee_id, $session->session_id, $thomas_name);
          if (count($committee[$session_name]['members']) > 0) {
            legislature_committee_membership($committee_id, $committee[$session_name]['members']);
          }

          // No, subcommittees are not actually implemented.
        }
      }
    }
  }
}
