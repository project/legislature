<?php

function legislature_sunlight_politicians() {
  $result = db_query('SELECT govtrack_id, politician_id FROM {legislature_politician} WHERE govtrack_id != 0');
  while ($politician = db_fetch_object($result)) {
    $response = drupal_http_request('http://services.sunlightlabs.com/api/legislators.get.xml?apikey='. variable_get('legislature_sunlight_api_key', '') .'&govtrack_id='. $politician->govtrack_id);
    if ($response->code == 200) {
      $xml = simplexml_load_string($response->data);
      $new_politician = array(
        'politician_id' => $politician->politician_id,
        'phone' => (string) $xml->legislator->phone,
        'email' => (string) $xml->legislator->email,
        'congresspedia_url' => (string) $xml->legislator->congresspedia_url,
      );
      legislature_politician($new_politician);
    }
    else {
      watchdog('legislature', t('Error importing Sunlight data for politician @politician_id: @error', array('@politician_id' => $politician->politician_id, '@error' => $response->data)));
    }
  }
}
