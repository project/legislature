<?php

function legislature_import_politicians($name) {
  $file = file_save_upload('data');
  $handle = fopen($file->filepath, 'r');

  // Check the header
  $header = array(
    'politician_id',
    'last_name',
    'full_name',
    'roster_name',
    'phone',
    'fax',
    'email',
    'party',
    'gender',
    'middle_name',
    'nick_name',
    'first_name',
    'name_modifier',
  );
  $row = fgetcsv($handle);
  foreach ($row as $n => $column) {
    if ($column !== $header[$n]) {
      drupal_set_message(t('Column headers are not correct.'), 'error');
      return;
    }
  }

  while ($row = fgetcsv($handle)) {
    $politician = array();
    foreach ($row as $n => $column) {
      $politician[$header[$n]] = trim($column);
    }
    legislature_politician($politician);
  }

  fclose($handle);
  file_delete($file->filepath);
}
