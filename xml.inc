<?php

/**
 * Parse a string of XML into a callback function.
 *
 * This is deprecated. Use SimpleXML.
 */
function legislature_xml_parse($data, $callback, $setup_arguments = NULL) {
  $return = TRUE;
  $parser = drupal_xml_parser_create($data);
  legislature_xml_set_callback($parser, $callback);
  if (!is_null($setup_arguments)) {
    $callback('setup', $parser, $setup_arguments);
  }
  xml_set_element_handler($parser, 'legislature_xml_start', 'legislature_xml_end');
  xml_set_character_data_handler($parser, 'legislature_xml_data');
  if (!xml_parse($parser, $data, 1)) {
    $return = FALSE;
  }
  xml_parser_free($parser);

  return $return;
}

function legislature_xml_start($parser, $name, $attributes) {
  $callback = legislature_xml_get_callback($parser);
  $callback('start', $parser, $name, $attributes);
}

function legislature_xml_end($parser, $name) {
  $callback = legislature_xml_get_callback($parser);
  $callback('end', $parser, $name);
}

function legislature_xml_data($parser, $data) {
  $callback = legislature_xml_get_callback($parser);
  $callback('data', $parser, $data);
}

/**
 * Set the XML callback for a given parser.
 */
function legislature_xml_set_callback($parser, $callback = NULL) {
  static $callbacks = array();

  if (!is_null($callback)) {
    $callbacks[''. $parser] = $callback;
  }

  return $callbacks[''. $parser];
}

/**
 * Return the XML callback for a given parser. 
 */
function legislature_xml_get_callback($parser) {
  return legislature_xml_set_callback($parser);
}
